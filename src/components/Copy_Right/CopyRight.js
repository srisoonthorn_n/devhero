import React from 'react';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

function CopyRight() {
    const classes = useStyles();
    return (
        <div className={classes.div}>
            <p className={classes.textCopyRight}>CopyRight © Dev Hero Co., Ltd. 2020., All Right Reserverd, Chiang Mai, Thailand.</p>
        </div>

    );
}

export default CopyRight;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        div: {
            padding: 8,
            left: 0,
            bottom: 0,
            backgroundColor: "#000000",
            textAlign: "center",
            height: 46
        },
        textCopyRight: {
            fontSize: 12,
            color: "#FFFFFF"
        }
    }),
);
