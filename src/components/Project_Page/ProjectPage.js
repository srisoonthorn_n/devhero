import React, { useState } from 'react';
import MenuBar from '../Menu_Bar/MenuBar';
import { Breadcrumbs, Link, Typography, Container, Grid, Button, Card, CardActionArea, CardMedia, CardContent, Fab, makeStyles } from '@material-ui/core';
import CopyRight from '../Copy_Right/CopyRight';
import Slider from "react-slick";
import { StyledProject } from './styledProject';

function ProjectPage(props) {

  const classes = useStyles();

  const [showList, setShowList] = useState(false);


  var settings = {
    dots: false,
    infinite: false,
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 380,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }

    ]

  };

  return (
    <div className={classes.divBody}>
      <MenuBar />
      <Grid className={classes.gridBreadcrumbs}>
        <Container className={classes.containerBreadcrumbs}>
          <Breadcrumbs className={classes.breadcrumbsStyle} aria-label="breadcrumb">
            <Link className={classes.linkStyle} href="/">
              Home
            </Link>
            <Typography className={classes.linkStyleProject}>Projects</Typography>
          </Breadcrumbs>
        </Container>
      </Grid>

      <Container className={classes.containerHeader}>

        <Grid container spacing={3} direction="row" className={classes.gridHeader}>
          <Grid item xs={6} lg={6} xl={5} className={classes.gridImagePhone}>
            <img src={require('../../images/screen.png')} className={classes.fixedImagePhone} />
          </Grid>

          <Grid container direction="column" alignItems="center" item xs={5} lg={5} xl={7} className={classes.gridHeaderText}>
            <Grid className={classes.gridTextInHeader}>
              <Grid item xs={11}>
                <p className={classes.HeaderText}>
                  Apps that help in management Evaluating the severity of symptoms
                  </p>
              </Grid>

              <div className={classes.line} />

              <Grid spacing={1} container direction="row" alignItems="center" className={classes.gridInHeader}>
                <Grid item={6}>
                  <img src={require('../../images/dasd.png')} className={classes.imgInHeader} />
                </Grid>
                <Grid item xs={6} >
                  <h1 className={classes.nameAppTextInHeader}>ER Speedy</h1>
                  <h3 className={classes.descriptionInHeader}>health care</h3>
                </Grid>
              </Grid>
            </Grid>

            <Grid className={classes.gridButton}>
              <Button className={classes.buttonSeeMore} variant="outlined">SEE MORE</Button>
            </Grid>
          </Grid>
        </Grid>

      </Container>


      <Grid className={classes.gridProject}>
        <Container className={classes.containerProject}>
          <div className={classes.divOurProject}>
            <h1 className={classes.ourProjectText}>Our Project</h1>
            <div className={classes.lineBottonOurProject} />
          </div>

          {
            showList == false ?

              <Grid item={12} className={classes.GirdCardList}>

                <StyledProject>
                  <Slider {...settings}>
                    {
                      [1, 2, 3, 4, 5, 6, 7, 8, 9].map((val) => (
                        <Grid itme xs={11} lg={11} xl={11} md={11} sm={11} >
                          <Card onClick={() => props.history.push('/partner')}>
                            <CardActionArea className={classes.cardActionAreaStyle}>
                              <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABFFBMVEUKHydf09Fh19Vg1tSs5f1i2tgIGyTS//8DFB4JHSZJu8IGGSIAAABPwcZSxchMvsTC9P8AABMADhq27P688P4AEBs/sLvM+//G9/8AAA9Yy8wACBZFtr85qrcAAAtdz81Wv75KpqZIoaE1eHpQs7IbQUZAkZINJS0yo7MjUlYxcHIUMzkeR0w7hocuaGsNJCwRLTNDl5gmWVw4f4FUurkYOj8kOUBVr8GAw9UmV1tPrLQ4jpRZvMWQ1Oeb0uiK1OR8scKSxNpljJxCYG2Musw5U16Ap7KSvMet3+9xlqEnc31Qa3IvjptquMmaw8mn0toySVFceoApgIwTZXGt2N1LorNed3qFp6odl6lrio8TS1lwmar+7xgWAAAXtUlEQVR4nO1dCVfbxtq2pIyYQYZEAgGSghZblvEmvCkJt226ZmmStkmb3tLc//8/vlmlkSzTNjVgf0fPOT0NGMw8et959xm3Wg0aNGjQoEGDBg0aNGjQoEGDBg0aNGjQoEGDBg0aNGjQ4N8A3vcCbhve8P85RXsOzBt/AOkGtC0MGxo6uqNVbRDGKFAzfd2rOvRgP7tKF5MkmSzSq6yPv7H2p7cTVqyq9WqKDAv2hrP40BUIXMWNO2kPWsbuyNIeAkVNvLpXvMEkxrQOZRCeSrCcjD37zpf6eUDI1RQtdFa+b8EhFh7mdFAg50hIDo37WO8/hzUBiqIEo/LWQnZrQehxZg8p9h/u/4dgn9IM3B0xwJ5CAMobERpdKjzC64vBePFw//GXP331/IHA8+dff+nGu6Gm9oKIUNEiyV8g7yp2Dwm9/f2Hv/qG4acSuxzf1O3d7YMTalSI7jS3jQb84pDw26dA5Pt6DcGjb1b27jbCGDCCCkiFmtqjx5zfY4wTqor2tzUMv9sJGXqJyhhqMV+vNT88zPk9PvnyPWZu9FcJPjj6ficsjc2VVFHUHrWmVpcJkPE7efbg237bhz8cUZQpPtmFyEbvKQLqjAjRnGCCTIAnJyfnePsdvf7m5fMjCbk9vbzv1f8dwBTkFBUch/mMIJPfyVOiixjHRxVQhj/49736v4N8G2Kfr0w8r3vINZQI8OmDKjOZ49F31n2v/u/AjMU2DAL38DI9ZBpK+EkEj2XkJF/sRNBmLLWcoOt2HjIVPZEleLwK+v3ncBeSC9QXpjSgKcSBUNHz8/MHnN8jSuoRQ8Hx+OVObEM0DQoRlghePGf8zr5/eXr2qARG8Xg3vCEaBVWC5+8owWdcQc+etJ+8elQhSSi+bt/34v8W9FGuo0KC5z2iok/zHfhEN9pPXh6fnp4R5BzPftyJkE3IkNhRypCoaPaG6yjdfWdPsMU0nNbbVx9P9xhNSvHschfsDNmHbkWEJydv/HfnPwmCj05fsO0GfeP9hz9+/uUjESAm+Go3lLSFWsSWMhE+pCJ8d3I+8t49EAQfnf4oUiRk2I7jWTa028dnp+93whmSWgzx+LId/XR+/g7Cbx8JB3H2sZoEIvvt3umrnUgNCbyOyhlSEWIdJRT77ZdUFzHBs70PVZPS/nh69mInXAWB3QWEYbELz703F09ff2i//+WMEjw7PeuXFbL9597eK3837AyGcaXmSkoIXpz/2v7t6+NHR69eYorEcp7ufbRkgbXfYov6ZEd2YYuEbS5lmIvwt5Nf298fYRU94wRPMcUnjsh1of/n3unejzsRsHE4kVBSSvDk06/nv394/4pvQkIQUzz9Y9r2bNtyzBe/YIIfd4lgCw41SUmxr++/eXZ29PqYEWQMT/f2Tn/+87///fDHxz38z73dMTME6JJZUqqkOFw7v756+/q4JEJMsIQ/dkqEOAeesW3Ic6aLi4vXR8dVEcr4uBOpvQQ9UySGmODFUYVhmeDpDtlRDicqM/ydB2xrRPh2N3IKGcZYMGRK+vS4zLCyCXck4i7Bmx0e7BcyvJmhuTPBjATUf3iwn8vw+ulx2ZSWCP5480TDtsKeV7V0LcP+LhTyV2E56cG+cBY3yvDnG13h9ipwMmhf5Qwvnh6t34cf2m3HWzdSg3r6dkoYpu7h48XVudiHFzd5i9OPf/z59v20VdeUMa4eX24lRT+mDd8TYUuvb/aHDH/WtPD13uFyGzVVHymHJY9//fomU0PkiAnWZcB6duBGW2hr7UmeWrB9eP3s+EYh4mTqQ63B0bPDQ6W7fVGrv3QrMvypYFi3E/fO3teXoUhw5EoDD1sCPQsqDC+EqSmEWOL4M1yTHsKh67raZNuECIeKyJ5yNf3qqCrEgujZh/Y6IVkdLEIl3LbUw5spcpmmpKYrFPdO/2itTS30ER0FU8Zb5jGcpVKqYhCPeH18VE/x7MkNVUTnC8JQmsvZDqBLt1SJEtZUpihxzGv5aIUpHFMRBqC7XQz1XiBXEznD31lnLacoOO79yXVU/1QZZWzB0T5TUnWxXfN8xqBcERa25rhCkZP8hfsJ49O7aUlUdvb4gDEE3e1iCOdAYigLMaeYcyQ0eZFG/3R9/cnLxWg46eODA0pw6/YhnRgqmmvyTiwoFhxPX7IiBhpdX1//+sm3IYS25ww6Dx9yESpgsF3uAg6BUhKioPhaliKnif/b40K03l1f4x/79c1vv7354mR/f18w1MItaw4zhszWPBSjQkJPj47LHAnNX5gQ4W80+jk/x7+AH4wgGPDpuC0Cm2srdbnPhduv5XjGujKozwgSiphgLkJFG2+XklJLIw2byHr6TJ6IkuZM3lJ7ar9hDKsi7Gxb+kS8BaV4ODiQhEgpPl8z9fWC6iG6PqcipAQfEkNKRKhs2S5kHp9i3p64FYoX+VRpZartLYnd9OxiVUfBYst2IYvayJD3zEHGvqynlOKz+snL4+9sSMI0ZmbkTRhul7enMMlwohZC1IJXLrOnf03x6NsXbQMHatePyzqqzrfL21N4M7wRwRVZmTMrKHJzc/HTAz4mvDIg/KLt2/ZvJ+TISa6j2+YpKOBQFQYQwZh7RSFGwrLmoAWb1H/+vxd9qz344nGuo8utnDjVM7wN+eFDo3eYz+mfnHCSTIy1HI8ePP/2hx++2WeOQguyLXOFHE4IZsKH2XO3OIogcVwnR4yvvuSeUFGvttDMEFgTUJwf9Ybu4cFBleL5+TqOX39JVZRuwuE2bkICfRRLxUGzSylKHAXPFZLPv/qS2BhBsLttwUwBoyen64SidOopJ0lY/uenZ8++fY7x7Nmzn5iXECqqbTFBLMXSV17qCjEyjhJL7OBPqAvEXh7z4xqKjYySbjPBKqwrdnaUnc57vIp9SvCg0FDVHWzrHqwHnMauOEC6X2G5v1/Ij54HxhIE8XQLQ5kboTsTl59wfihYCtAYRuanKgtny2rAfwfeOGZn1ItTzhziQDfjF2hq3NstDRUwrDTMz+FXT6vn8lNAmHrbGcisQNerC7Vbw1Cp3jaQn8anFhSEQ31L45gqdC/rDazKbkLQmsf8PJTLmEn0FFWN5569jaH2KnRvmgCgRtMVfYPOaBHTIeIC1L+rwXIx9XfEgiJrmgSkHKUqNV5Nt51R+kUcuhqDqqpBGCfpyLF3xYB6rdTlB2W1MK3r3+rQ8i6z8WCeYswH46xlWjcmgmibbrAxnEmQH5PFpmNdFQLphmFAAuOGG3iQAS3TaRvZCG2JgHWvF0oHnbGidj7btxm2p08Hw6QTB3hPx2gbbCwyp4mmKSWGn1doMaAHx90oVDE3lb6jGs7vX4qwH7mqUsZaLb0ByDMG3VmogdLT0sD0vinavbDKTwHRP58UgShxNaBqK++12ge/W5hXWnVRKojWmkhU9yXC9scaB6BKjhKsvZXpDmFNghJBTdXcTrpu0AKhkWw4EJzatmXal6PeYL5c0QSiosrEuV8RmhNQ5hdOBtBcuwftjjb3iLOwLc90YGsYdIZJFIfYaoKKJmgqUMLlJPPumWC3RBDEc6OUIVTuKUMtHNHMx91hdzGZRWHo4pwJ77wVNSeaEE/mvb5ZDXHvGvBK1ixNmXtl6VmD8vQdOSysJjEWGHEGq8TENg6i2ciz4P1HNPpI2oOaFo4qBtRIQVyarqTnTMOwnphAsLDapqfftwWlsKJChCAeVEsQeoa1rTTX5NHbQYJVViwSZ/92Dcc0TRv175RLLXjXl62wa65mTKTvXRqKQfTujKCinQDgJCNKJl0u3KjTiZbLMBzfe05ViFAL6oqA9GSwFheqy+9dcEO8CzEA+3XQzUaXtul5lrhniu5RbJan9zhgqmMjwE7gM4K9uvDYi8jralEIZ+Ma2nI663RmySKdsM5/26B7TrrCh7+te1+XDeqWlWU29t75fS1ZrT6xXacmuSQ45QTLC8NySq/r09XtCeb3QdEwe4tY0SId6Zwh6NXf59mnjX01EgqMLunPi1sHjR79UuOziSsixL8a9O7UHRpkpbo/iBSA/XQHtiC7VUgL64/ZGWMaDRSmxmNaqfJxNUi3scYfAH8espKCaHSntgYOIw8ZvRiHG6oakeTIYjdDrRuwYyNExVWYep/r9Ih6OqNXegC2uCqMmBlqh9z0rgIaw6KVPisBEbKWeB1uYrXJ3qGjCXiNa6az6KAbljBfJjJjJsIJExrTSi3sU756JgwpDuewu+gkV3dVgIPWYLK4hFQkIGqTJz0fRh2yQfgNbeqsXfusOUPuLZDYZy4PVny2S4fMCts8wAVJ2zOpJbobG4NsJ41VAMIBxNtKU7RLIjecBoAQFrfsaWmGY5CVc2iCIRWZbibC+zHGBtdhPttl8EDHlRNL47ZZIthfxCrJYjSlp6PLUFPnPmaIKce05ss3ooJjks4kHY9alo/hOJ7McEmUzc6jA5c3QB3mOfipEb0HCv7Y1SJkeL45GNxuFQqagyVO2rTOtKNhe2D4S01N29hDJOm0rfeh4EDFSGwDCUQCN1wuZzSn40ocXkLPSkUhR9RvUIsKTeMi05ndVUJIYhto9KeD2RI/ytvtl85jkpQGfccbAnXSzvBDB12cTSzbrUGiBMQGFtddSkZe0wCdG+K2FMwHSVEpFlEKGS8ivpKPNqAp+4kgjjpR7NL0ioaw3dszp14Hez0cJLttHGEDnI8qeElq2sOrjFz858EEaxAc1FQeqL0w852mqEV1yZ2LJj2zw4UZhkH+fOS0UVNvb9YbzrHH7YVa6LeQDkjSjbMedexgk4/9FQi79C973Wrxga8M21o0rUoYpLnSOXTMbylOrCMrrk+HQXJ70TfMMs8LVcKwRXIEN+uoauIMsCkNJwOD2wBrENVVyKgfh/PKNzvFuI1J2JdC1lpt0KpXZ28Uut6yQpUMAUEyOG95c2pwvJHtW0SASMdARnsGVsulS1K9MOalF0BpnogwlMrGMK2ToRrcdo5ohio5rApdBeBIxBrMiZYh+hEApjHNer1sCh0/7bjlOjWIWMPMk1RPU2eyq7NIjArmhak0Z2VdIFshiG59NMNZUrNodwANPpkHhp7XGyZxSI5EYOcQdTMfDbrRklQEMRRVW3g2VT94lXsT0qaXAwKD1rCCogCAWkkAWExK3kUL49kwM289MPVmIGwTB9wVbgyaVjoLFVICFM8aLNvIsLFMe1dp9yobZSPbTGeUojdhNTWAvUSlCu4RNdWColCFrFGadHBMmgzH2SC7tO7kgyH0TAEujquQTaSHdROlkaJWWwuAtUPJZ3Pg0A0D5wmAxpvI65GaS12bHuIgQAOxPOKF359mxvhNigjQsL1b/XAIOIgSns15MBtGWq1zCCrdIRhomstzBs8zndoaOLQTN5IsJbTsGtdnD+LZYHUsYIMwPIv9L+0slXrfJ6fxbK3EhoK/PKeMLL/YZwgO4yhbudDF6CkkU8xWpjuszZog2xnjkGYNPYrS1DLPhPqryrW2yqvDmIRQSbWnbdGcUnPD8k0o5uWkdlDgM2H7XZxV3ECPpEhyEmAvaMzZWSluoP7U92r7bn7MYtiw/GFKuTFW5do59GfY7I43dU8Y8obuOu0sUDruCbu8PlFNDMwIxLO0ZlflzlAlmWgBX9wvrahX4rd0L41cEi5E3kZsLTI6tX3LCkqDCeJDErTKGVAiEezHtZVgGhbBjwakBwDH+d8WiWTLyyL+wNVoI5f4OHEtQY0WrIs8QO1KDO2EM1yWxn2R4WorT4O+0JJCdG2ZOweEchGKEFZ3FsVMCwg30L2xFjUESVlj1l0EpNvQEXdcFw4D9V3OsLQC5HfUmm+3aFwgvXtxpMuWmpOsNgz7pQe+iYPCRrVPhIMqt5tOLRyX0Uauz7spquS7zbwWXrg77LdzdS995pWOYI8/EP4XXHEsz4xyEbI97Q2Cciz/789gon6VYDQcO/KkFhQvhIVpc0TALSW43bgYSJEfPcr6XHm1Je9JCauFUFEjBlh1dW9R7qmqG7jCxhhXGca6XwrADFHmlBjmnyGQZ0d2KVGW0kJ9hH0dr5zbmfgX02KjaDADHObCfiUTVd0NHKOF3eo21IKoK9+VCwXDZZH8OcJAiGsf8g6HeEx5mkGqVeyloGfATDwYVj/IGxlaaOjOvDKVBJb1/aB/BulzR4r1AbfvFw2z/LNlCo2xBB8hrNKHe5DF5d19S7AAV3YLtVkRnf2aZGHB0HSSckxF6kSbiFZXtJS9uxLn3SbBUHIBRY9FOLEiExY7LW/WcGOiJg6y4ZCZLTYTJz0Wtz8qzwVq6rK3mUsJjTW1NFURWyBnWEwu6SNhgLlJ4W00slRGNf/Mq6IABb3WULBgD8DLLakWD8qNcdUdrvbUPxNeUh/RiGO6pAxXEhcllDNk0YvVEao4FHIRj4MXEbV4PAlzY0Q7ivq0eLhhiaCmzjbZ/vZm1bkCzohZA171lTwAwoF6/nEsrI9mi9+JfP4hGOqc5e/osgjLCt9HH5Y1WRPs49B1s7dm+mmormT1dPOzByDEM+ZVAGMYFiaQstYzQaOvm6xoqEbtKSn6GONVFeGTen61YSpejoxNV6egn3VnS3pfjMzUpWWK/EmDS92wvVHakdJkjZUPxVMg5TqY8p9fggSntfaKN1KDIf2lSiWyINi5jbapbnuOdzkad6NJnJPUXFJWzK29cmmNF7FSKp1y62OJxllf16Etqvc4uk0yp1PioYFAbDGzU8dQU7q3d0k9wjIyPXuchHny4kuhoxaHK0UOdUFWy3owpBfaNkfpRAp0wbwtRwIaCCcjnvOhy5rBKWxDV1LOzcPwjHnMtW4IkZUvpGYMj1l9nzuEoBMHiipJBqewvlQyVuMrmGtgrs0lCcb9u2l8wzavwAemPbkpPQ5IhloEDZVnAGKI7OJLLfKluTizro2h3tn5WcgXHaCaGRhpQbQ5uO5HNNJGbUu6iLOyligCk35zze+4t1pWLKAjoVtTc3YDQzoZxO8DwaurvopXi/QSDxAm8wwbNRta49r3VeONe4o6IBiLP99rV7t+mgpyKgFJI0V0CUQ0o6liF9srQSF+LXDj2WI4X+Pu1eXYv/X7JEg3TfzBTC9bPJx2TKbjPI6jaXnEazNOTLsuWtzlk1E4VBfusFSrZCMBtQTpnFSKLI8BWrdA1vDGRR4amN3y0sLUsSwxvUAzBMTH8cDA7i/d5WyIHIc9AhIO8HhBTRbrBqLFWytFGAFAGHU6s9kscRNj095fN3udwt6rnbZsRbClIKX2PMqhGSOf1iBjQ8jxHWxJvBaTKmHIP14XDNvz8IaarBakPTk6ZlNhWNBg2d+o6TGcbKbI/qznF6UiBfBopLCdKkKioJHnVqg/c8XkkM1bbETbYWsRrraSOSG3Z9nj2gRADTfVBCcDPO1xpJRissiHkk2f82ikyAkIQx4/F+1syD0oC/o8sqdpAQTZxoDU6mt4qGMy9dFbOVpFX4s34CINHemt6Txxy89YC6Z56pQH2S2S/+azM34LjbhlLeqHPpuU0ihnY0yaiCyLJFNQ44SMCZUNDWDFZtiqq01v4tY6vReGoQtWnq6a2sZVkdMVfQtrIvwD2ZWMD08UKZEpfis14AGmPejEWdHyMCy/P18kyVIK5iJ+Kkj3qicAFWUjNWGsSHWGjj5ZaRJB2g8WCQM0luahaaCSgFkqkLYGUdjJRJJueJX+CiL3Q46L+ECqKptX1b3KM9B/B6nwvKI6xkD8JbkdirA+AXEQ2BjFYFmaTkOGpf9F1wi1coZiOpMC26OZvB03c9tSXVypKnNaSNAzVmVXg1I3FHnpPG8T6rZh/dNlIFsohxqVKhbIdsiQMhsSxNnWRqakV+slmhqLI3YG7JCjE52qW4LS158Td4h2mxau/Lru6OmkEy/DcNndzDCKVQ2uAfbsOQEEF51ksPkTgswPaVrdKQAELc9xfN/Z0PBpMUPK+AVJuZRn38q8sjXB5k1V7uTOLzL8nOsnCCbTuznx6I1DEG+iMfE3YCaAX/KgRHP/NsL5WhiOflfX8SCjE8dhOOsO+86dHvC4u/OHyDMdB6cFdzFm1qBBgwYNGjRo0KBBgwYNGjRo0KBBgwYNGjRo0KBBgwYNPhP/B4nry3Ic4oqZAAAAAElFTkSuQmCC"}
                                className={classes.imgCardList} />

                              <CardContent>
                                <Typography gutterBottom component="h2" className={classes.HeaderCard}>
                                  {
                                    val % 2 == 0 ? "Er Speedy" : "asdfasdfasdfasdfasdfasdfasdfasdfasdsadasdasdasdsadsadsadsadsadsadsadsadsadasdassadsadsadasdasdasdsd"
                                  }

                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p" className={classes.descriptionCard}>
                                  {
                                    val % 2 == 1 ? "Er Speedy" : "asdfasdfasdfasdfasdfasdfasdfsadfasdfasdfasdfasdfasdfasdfasdfasdfasdf"
                                  }

                                </Typography>
                              </CardContent>
                            </CardActionArea>

                          </Card>
                        </Grid>

                      ))
                    }

                  </Slider>
                </StyledProject>
                <Grid className={classes.seeMore}>
                  <Button
                    onClick={() => setShowList(true)}
                    size="small"
                    className={classes.onMoreLessButton}>
                    <p className={classes.onMoreLessText}>See more</p>
                  </Button>
                </Grid>

              </Grid>
              :
              <Grid container direction="column" spacing={3}>

                <Grid item xs={12} spacing={2} container direction="row" justify="flex-start" alignItems="center">

                  {
                    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13].map((val) => (
                      <Grid item xs={6} md={3} sm={4} lg={2}>
                        <Card onClick={() => props.history.push('/partner')} >
                          <CardActionArea className={classes.actionAreaCard}>
                            <CardMedia
                              className={classes.imgCard}
                              image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABFFBMVEUKHydf09Fh19Vg1tSs5f1i2tgIGyTS//8DFB4JHSZJu8IGGSIAAABPwcZSxchMvsTC9P8AABMADhq27P688P4AEBs/sLvM+//G9/8AAA9Yy8wACBZFtr85qrcAAAtdz81Wv75KpqZIoaE1eHpQs7IbQUZAkZINJS0yo7MjUlYxcHIUMzkeR0w7hocuaGsNJCwRLTNDl5gmWVw4f4FUurkYOj8kOUBVr8GAw9UmV1tPrLQ4jpRZvMWQ1Oeb0uiK1OR8scKSxNpljJxCYG2Musw5U16Ap7KSvMet3+9xlqEnc31Qa3IvjptquMmaw8mn0toySVFceoApgIwTZXGt2N1LorNed3qFp6odl6lrio8TS1lwmar+7xgWAAAXtUlEQVR4nO1dCVfbxtq2pIyYQYZEAgGSghZblvEmvCkJt226ZmmStkmb3tLc//8/vlmlkSzTNjVgf0fPOT0NGMw8et959xm3Wg0aNGjQoEGDBg0aNGjQoEGDBg0aNGjQoEGDBg0aNGjQ4N8A3vcCbhve8P85RXsOzBt/AOkGtC0MGxo6uqNVbRDGKFAzfd2rOvRgP7tKF5MkmSzSq6yPv7H2p7cTVqyq9WqKDAv2hrP40BUIXMWNO2kPWsbuyNIeAkVNvLpXvMEkxrQOZRCeSrCcjD37zpf6eUDI1RQtdFa+b8EhFh7mdFAg50hIDo37WO8/hzUBiqIEo/LWQnZrQehxZg8p9h/u/4dgn9IM3B0xwJ5CAMobERpdKjzC64vBePFw//GXP331/IHA8+dff+nGu6Gm9oKIUNEiyV8g7yp2Dwm9/f2Hv/qG4acSuxzf1O3d7YMTalSI7jS3jQb84pDw26dA5Pt6DcGjb1b27jbCGDCCCkiFmtqjx5zfY4wTqor2tzUMv9sJGXqJyhhqMV+vNT88zPk9PvnyPWZu9FcJPjj6ficsjc2VVFHUHrWmVpcJkPE7efbg237bhz8cUZQpPtmFyEbvKQLqjAjRnGCCTIAnJyfnePsdvf7m5fMjCbk9vbzv1f8dwBTkFBUch/mMIJPfyVOiixjHRxVQhj/49736v4N8G2Kfr0w8r3vINZQI8OmDKjOZ49F31n2v/u/AjMU2DAL38DI9ZBpK+EkEj2XkJF/sRNBmLLWcoOt2HjIVPZEleLwK+v3ncBeSC9QXpjSgKcSBUNHz8/MHnN8jSuoRQ8Hx+OVObEM0DQoRlghePGf8zr5/eXr2qARG8Xg3vCEaBVWC5+8owWdcQc+etJ+8elQhSSi+bt/34v8W9FGuo0KC5z2iok/zHfhEN9pPXh6fnp4R5BzPftyJkE3IkNhRypCoaPaG6yjdfWdPsMU0nNbbVx9P9xhNSvHschfsDNmHbkWEJydv/HfnPwmCj05fsO0GfeP9hz9+/uUjESAm+Go3lLSFWsSWMhE+pCJ8d3I+8t49EAQfnf4oUiRk2I7jWTa028dnp+93whmSWgzx+LId/XR+/g7Cbx8JB3H2sZoEIvvt3umrnUgNCbyOyhlSEWIdJRT77ZdUFzHBs70PVZPS/nh69mInXAWB3QWEYbELz703F09ff2i//+WMEjw7PeuXFbL9597eK3837AyGcaXmSkoIXpz/2v7t6+NHR69eYorEcp7ufbRkgbXfYov6ZEd2YYuEbS5lmIvwt5Nf298fYRU94wRPMcUnjsh1of/n3unejzsRsHE4kVBSSvDk06/nv394/4pvQkIQUzz9Y9r2bNtyzBe/YIIfd4lgCw41SUmxr++/eXZ29PqYEWQMT/f2Tn/+87///fDHxz38z73dMTME6JJZUqqkOFw7v756+/q4JEJMsIQ/dkqEOAeesW3Ic6aLi4vXR8dVEcr4uBOpvQQ9UySGmODFUYVhmeDpDtlRDicqM/ydB2xrRPh2N3IKGcZYMGRK+vS4zLCyCXck4i7Bmx0e7BcyvJmhuTPBjATUf3iwn8vw+ulx2ZSWCP5480TDtsKeV7V0LcP+LhTyV2E56cG+cBY3yvDnG13h9ipwMmhf5Qwvnh6t34cf2m3HWzdSg3r6dkoYpu7h48XVudiHFzd5i9OPf/z59v20VdeUMa4eX24lRT+mDd8TYUuvb/aHDH/WtPD13uFyGzVVHymHJY9//fomU0PkiAnWZcB6duBGW2hr7UmeWrB9eP3s+EYh4mTqQ63B0bPDQ6W7fVGrv3QrMvypYFi3E/fO3teXoUhw5EoDD1sCPQsqDC+EqSmEWOL4M1yTHsKh67raZNuECIeKyJ5yNf3qqCrEgujZh/Y6IVkdLEIl3LbUw5spcpmmpKYrFPdO/2itTS30ER0FU8Zb5jGcpVKqYhCPeH18VE/x7MkNVUTnC8JQmsvZDqBLt1SJEtZUpihxzGv5aIUpHFMRBqC7XQz1XiBXEznD31lnLacoOO79yXVU/1QZZWzB0T5TUnWxXfN8xqBcERa25rhCkZP8hfsJ49O7aUlUdvb4gDEE3e1iCOdAYigLMaeYcyQ0eZFG/3R9/cnLxWg46eODA0pw6/YhnRgqmmvyTiwoFhxPX7IiBhpdX1//+sm3IYS25ww6Dx9yESpgsF3uAg6BUhKioPhaliKnif/b40K03l1f4x/79c1vv7354mR/f18w1MItaw4zhszWPBSjQkJPj47LHAnNX5gQ4W80+jk/x7+AH4wgGPDpuC0Cm2srdbnPhduv5XjGujKozwgSiphgLkJFG2+XklJLIw2byHr6TJ6IkuZM3lJ7ar9hDKsi7Gxb+kS8BaV4ODiQhEgpPl8z9fWC6iG6PqcipAQfEkNKRKhs2S5kHp9i3p64FYoX+VRpZartLYnd9OxiVUfBYst2IYvayJD3zEHGvqynlOKz+snL4+9sSMI0ZmbkTRhul7enMMlwohZC1IJXLrOnf03x6NsXbQMHatePyzqqzrfL21N4M7wRwRVZmTMrKHJzc/HTAz4mvDIg/KLt2/ZvJ+TISa6j2+YpKOBQFQYQwZh7RSFGwrLmoAWb1H/+vxd9qz344nGuo8utnDjVM7wN+eFDo3eYz+mfnHCSTIy1HI8ePP/2hx++2WeOQguyLXOFHE4IZsKH2XO3OIogcVwnR4yvvuSeUFGvttDMEFgTUJwf9Ybu4cFBleL5+TqOX39JVZRuwuE2bkICfRRLxUGzSylKHAXPFZLPv/qS2BhBsLttwUwBoyen64SidOopJ0lY/uenZ8++fY7x7Nmzn5iXECqqbTFBLMXSV17qCjEyjhJL7OBPqAvEXh7z4xqKjYySbjPBKqwrdnaUnc57vIp9SvCg0FDVHWzrHqwHnMauOEC6X2G5v1/Ij54HxhIE8XQLQ5kboTsTl59wfihYCtAYRuanKgtny2rAfwfeOGZn1ItTzhziQDfjF2hq3NstDRUwrDTMz+FXT6vn8lNAmHrbGcisQNerC7Vbw1Cp3jaQn8anFhSEQ31L45gqdC/rDazKbkLQmsf8PJTLmEn0FFWN5569jaH2KnRvmgCgRtMVfYPOaBHTIeIC1L+rwXIx9XfEgiJrmgSkHKUqNV5Nt51R+kUcuhqDqqpBGCfpyLF3xYB6rdTlB2W1MK3r3+rQ8i6z8WCeYswH46xlWjcmgmibbrAxnEmQH5PFpmNdFQLphmFAAuOGG3iQAS3TaRvZCG2JgHWvF0oHnbGidj7btxm2p08Hw6QTB3hPx2gbbCwyp4mmKSWGn1doMaAHx90oVDE3lb6jGs7vX4qwH7mqUsZaLb0ByDMG3VmogdLT0sD0vinavbDKTwHRP58UgShxNaBqK++12ge/W5hXWnVRKojWmkhU9yXC9scaB6BKjhKsvZXpDmFNghJBTdXcTrpu0AKhkWw4EJzatmXal6PeYL5c0QSiosrEuV8RmhNQ5hdOBtBcuwftjjb3iLOwLc90YGsYdIZJFIfYaoKKJmgqUMLlJPPumWC3RBDEc6OUIVTuKUMtHNHMx91hdzGZRWHo4pwJ77wVNSeaEE/mvb5ZDXHvGvBK1ixNmXtl6VmD8vQdOSysJjEWGHEGq8TENg6i2ciz4P1HNPpI2oOaFo4qBtRIQVyarqTnTMOwnphAsLDapqfftwWlsKJChCAeVEsQeoa1rTTX5NHbQYJVViwSZ/92Dcc0TRv175RLLXjXl62wa65mTKTvXRqKQfTujKCinQDgJCNKJl0u3KjTiZbLMBzfe05ViFAL6oqA9GSwFheqy+9dcEO8CzEA+3XQzUaXtul5lrhniu5RbJan9zhgqmMjwE7gM4K9uvDYi8jralEIZ+Ma2nI663RmySKdsM5/26B7TrrCh7+te1+XDeqWlWU29t75fS1ZrT6xXacmuSQ45QTLC8NySq/r09XtCeb3QdEwe4tY0SId6Zwh6NXf59mnjX01EgqMLunPi1sHjR79UuOziSsixL8a9O7UHRpkpbo/iBSA/XQHtiC7VUgL64/ZGWMaDRSmxmNaqfJxNUi3scYfAH8espKCaHSntgYOIw8ZvRiHG6oakeTIYjdDrRuwYyNExVWYep/r9Ih6OqNXegC2uCqMmBlqh9z0rgIaw6KVPisBEbKWeB1uYrXJ3qGjCXiNa6az6KAbljBfJjJjJsIJExrTSi3sU756JgwpDuewu+gkV3dVgIPWYLK4hFQkIGqTJz0fRh2yQfgNbeqsXfusOUPuLZDYZy4PVny2S4fMCts8wAVJ2zOpJbobG4NsJ41VAMIBxNtKU7RLIjecBoAQFrfsaWmGY5CVc2iCIRWZbibC+zHGBtdhPttl8EDHlRNL47ZZIthfxCrJYjSlp6PLUFPnPmaIKce05ss3ooJjks4kHY9alo/hOJ7McEmUzc6jA5c3QB3mOfipEb0HCv7Y1SJkeL45GNxuFQqagyVO2rTOtKNhe2D4S01N29hDJOm0rfeh4EDFSGwDCUQCN1wuZzSn40ocXkLPSkUhR9RvUIsKTeMi05ndVUJIYhto9KeD2RI/ytvtl85jkpQGfccbAnXSzvBDB12cTSzbrUGiBMQGFtddSkZe0wCdG+K2FMwHSVEpFlEKGS8ivpKPNqAp+4kgjjpR7NL0ioaw3dszp14Hez0cJLttHGEDnI8qeElq2sOrjFz858EEaxAc1FQeqL0w852mqEV1yZ2LJj2zw4UZhkH+fOS0UVNvb9YbzrHH7YVa6LeQDkjSjbMedexgk4/9FQi79C973Wrxga8M21o0rUoYpLnSOXTMbylOrCMrrk+HQXJ70TfMMs8LVcKwRXIEN+uoauIMsCkNJwOD2wBrENVVyKgfh/PKNzvFuI1J2JdC1lpt0KpXZ28Uut6yQpUMAUEyOG95c2pwvJHtW0SASMdARnsGVsulS1K9MOalF0BpnogwlMrGMK2ToRrcdo5ohio5rApdBeBIxBrMiZYh+hEApjHNer1sCh0/7bjlOjWIWMPMk1RPU2eyq7NIjArmhak0Z2VdIFshiG59NMNZUrNodwANPpkHhp7XGyZxSI5EYOcQdTMfDbrRklQEMRRVW3g2VT94lXsT0qaXAwKD1rCCogCAWkkAWExK3kUL49kwM289MPVmIGwTB9wVbgyaVjoLFVICFM8aLNvIsLFMe1dp9yobZSPbTGeUojdhNTWAvUSlCu4RNdWColCFrFGadHBMmgzH2SC7tO7kgyH0TAEujquQTaSHdROlkaJWWwuAtUPJZ3Pg0A0D5wmAxpvI65GaS12bHuIgQAOxPOKF359mxvhNigjQsL1b/XAIOIgSns15MBtGWq1zCCrdIRhomstzBs8zndoaOLQTN5IsJbTsGtdnD+LZYHUsYIMwPIv9L+0slXrfJ6fxbK3EhoK/PKeMLL/YZwgO4yhbudDF6CkkU8xWpjuszZog2xnjkGYNPYrS1DLPhPqryrW2yqvDmIRQSbWnbdGcUnPD8k0o5uWkdlDgM2H7XZxV3ECPpEhyEmAvaMzZWSluoP7U92r7bn7MYtiw/GFKuTFW5do59GfY7I43dU8Y8obuOu0sUDruCbu8PlFNDMwIxLO0ZlflzlAlmWgBX9wvrahX4rd0L41cEi5E3kZsLTI6tX3LCkqDCeJDErTKGVAiEezHtZVgGhbBjwakBwDH+d8WiWTLyyL+wNVoI5f4OHEtQY0WrIs8QO1KDO2EM1yWxn2R4WorT4O+0JJCdG2ZOweEchGKEFZ3FsVMCwg30L2xFjUESVlj1l0EpNvQEXdcFw4D9V3OsLQC5HfUmm+3aFwgvXtxpMuWmpOsNgz7pQe+iYPCRrVPhIMqt5tOLRyX0Uauz7spquS7zbwWXrg77LdzdS995pWOYI8/EP4XXHEsz4xyEbI97Q2Cciz/789gon6VYDQcO/KkFhQvhIVpc0TALSW43bgYSJEfPcr6XHm1Je9JCauFUFEjBlh1dW9R7qmqG7jCxhhXGca6XwrADFHmlBjmnyGQZ0d2KVGW0kJ9hH0dr5zbmfgX02KjaDADHObCfiUTVd0NHKOF3eo21IKoK9+VCwXDZZH8OcJAiGsf8g6HeEx5mkGqVeyloGfATDwYVj/IGxlaaOjOvDKVBJb1/aB/BulzR4r1AbfvFw2z/LNlCo2xBB8hrNKHe5DF5d19S7AAV3YLtVkRnf2aZGHB0HSSckxF6kSbiFZXtJS9uxLn3SbBUHIBRY9FOLEiExY7LW/WcGOiJg6y4ZCZLTYTJz0Wtz8qzwVq6rK3mUsJjTW1NFURWyBnWEwu6SNhgLlJ4W00slRGNf/Mq6IABb3WULBgD8DLLakWD8qNcdUdrvbUPxNeUh/RiGO6pAxXEhcllDNk0YvVEao4FHIRj4MXEbV4PAlzY0Q7ivq0eLhhiaCmzjbZ/vZm1bkCzohZA171lTwAwoF6/nEsrI9mi9+JfP4hGOqc5e/osgjLCt9HH5Y1WRPs49B1s7dm+mmormT1dPOzByDEM+ZVAGMYFiaQstYzQaOvm6xoqEbtKSn6GONVFeGTen61YSpejoxNV6egn3VnS3pfjMzUpWWK/EmDS92wvVHakdJkjZUPxVMg5TqY8p9fggSntfaKN1KDIf2lSiWyINi5jbapbnuOdzkad6NJnJPUXFJWzK29cmmNF7FSKp1y62OJxllf16Etqvc4uk0yp1PioYFAbDGzU8dQU7q3d0k9wjIyPXuchHny4kuhoxaHK0UOdUFWy3owpBfaNkfpRAp0wbwtRwIaCCcjnvOhy5rBKWxDV1LOzcPwjHnMtW4IkZUvpGYMj1l9nzuEoBMHiipJBqewvlQyVuMrmGtgrs0lCcb9u2l8wzavwAemPbkpPQ5IhloEDZVnAGKI7OJLLfKluTizro2h3tn5WcgXHaCaGRhpQbQ5uO5HNNJGbUu6iLOyligCk35zze+4t1pWLKAjoVtTc3YDQzoZxO8DwaurvopXi/QSDxAm8wwbNRta49r3VeONe4o6IBiLP99rV7t+mgpyKgFJI0V0CUQ0o6liF9srQSF+LXDj2WI4X+Pu1eXYv/X7JEg3TfzBTC9bPJx2TKbjPI6jaXnEazNOTLsuWtzlk1E4VBfusFSrZCMBtQTpnFSKLI8BWrdA1vDGRR4amN3y0sLUsSwxvUAzBMTH8cDA7i/d5WyIHIc9AhIO8HhBTRbrBqLFWytFGAFAGHU6s9kscRNj095fN3udwt6rnbZsRbClIKX2PMqhGSOf1iBjQ8jxHWxJvBaTKmHIP14XDNvz8IaarBakPTk6ZlNhWNBg2d+o6TGcbKbI/qznF6UiBfBopLCdKkKioJHnVqg/c8XkkM1bbETbYWsRrraSOSG3Z9nj2gRADTfVBCcDPO1xpJRissiHkk2f82ikyAkIQx4/F+1syD0oC/o8sqdpAQTZxoDU6mt4qGMy9dFbOVpFX4s34CINHemt6Txxy89YC6Z56pQH2S2S/+azM34LjbhlLeqHPpuU0ihnY0yaiCyLJFNQ44SMCZUNDWDFZtiqq01v4tY6vReGoQtWnq6a2sZVkdMVfQtrIvwD2ZWMD08UKZEpfis14AGmPejEWdHyMCy/P18kyVIK5iJ+Kkj3qicAFWUjNWGsSHWGjj5ZaRJB2g8WCQM0luahaaCSgFkqkLYGUdjJRJJueJX+CiL3Q46L+ECqKptX1b3KM9B/B6nwvKI6xkD8JbkdirA+AXEQ2BjFYFmaTkOGpf9F1wi1coZiOpMC26OZvB03c9tSXVypKnNaSNAzVmVXg1I3FHnpPG8T6rZh/dNlIFsohxqVKhbIdsiQMhsSxNnWRqakV+slmhqLI3YG7JCjE52qW4LS158Td4h2mxau/Lru6OmkEy/DcNndzDCKVQ2uAfbsOQEEF51ksPkTgswPaVrdKQAELc9xfN/Z0PBpMUPK+AVJuZRn38q8sjXB5k1V7uTOLzL8nOsnCCbTuznx6I1DEG+iMfE3YCaAX/KgRHP/NsL5WhiOflfX8SCjE8dhOOsO+86dHvC4u/OHyDMdB6cFdzFm1qBBgwYNGjRo0KBBgwYNGjRo0KBBgwYNGjRo0KBBgwYNPhP/B4nry3Ic4oqZAAAAAElFTkSuQmCC"
                            />
                            <CardContent>
                              <Typography gutterBottom component="h2" className={classes.HeaderCard}>
                                {
                                  val % 2 == 0 ? "Er Speedy" : "asdfasdfasdfasdfasdfasdfasdfasdfasdsadasdasdasdsadsadsadsadsadsadsadsadsadasdassadsadsadasdasdasdsd"
                                }

                              </Typography>
                              <Typography variant="body2" color="textSecondary" component="p" className={classes.descriptionCard}>
                                {
                                  val % 2 == 1 ? "Er Speedy" : "asdfasdfasdfasdfasdfasdfasdfsadfasdfasdfasdfasdfasdfasdfasdfasdfasdf"
                                }

                              </Typography>
                            </CardContent>
                          </CardActionArea>
                        </Card>
                      </Grid>)
                    )
                  }

                </Grid>
                <Grid className={classes.onLess}>
                  <Button
                    onClick={() => setShowList(false)}
                    size="small"
                    className={classes.onMoreLessButton}>
                    <p className={classes.onMoreLessText}>On less</p>
                  </Button>
                </Grid>
              </Grid>
          }

        </Container>

      </Grid>

      <Fab className={classes.fixedChat}>
        <img src={"https://dk.lnwfile.com/_/dk/_raw/w6/s9/nj.png"} className={classes.messengerImg} />
      </Fab>


      <CopyRight />


    </div >

  );
}

export default ProjectPage;

const useStyles = makeStyles((theme) => ({
  fab: {
    margin: theme.spacing(2),
  },
  fixedChat: {
    position: 'fixed',
    width: 60,
    height: 60,
    bottom: 29,
    right: 10,

  },
  gridImagePhone: {
    textAlign: "right",
    paddingRight: "min(max(42px,12px),5.5vw) !important"
  },
  fixedImagePhone: {
    width: "32vw",
    height: "64vw",
    maxHeight: 445,
    maxWidth: 220
  },
  buttonSeeMore: {
    color: "#FFFFFF",
    borderColor: "#FFFFFF",
    maxWidth: 155,
    maxHeight: 49,
    fontSize: "1vw",
    fontFamily: "Prompt, sans-serif",
    width: "25vw",
    height: "7vw"
  },
  line: {
    width: "20.5vw",
    height: 3,
    maxWidth: 132,
    backgroundColor: "#FFFFFF"
  },
  messengerImg: {
    maxHeight: "100%",
    maxWidth: "100%",
    borderRadius: 100
  },
  descriptionCard: {
    fontFamily: "Prompt, sans-serif",
    fontSize: "min(max(0.1vw,12px),3.5vw)",
    lineBreak: "anywhere",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap"
  },
  HeaderCard: {
    // fontSize: "min(max(1.1vw,16px),3vw)",
    fontFamily: "Prompt, sans-serif",
    fontWeight: "bold",
    lineBreak: "anywhere",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap"
  },
  imgCard: {
    width: '100%',
    height: "30vw",
    maxHeight: 164
  },
  actionAreaCard: {
    padding: 12,
    maxHeight: 265
  },
  ourProjectText: {
    marginBottom: 5,
    fontSize: "min(max(1.5vw,25px),4vw)",
    color: "#414141",
    fontFamily: "Prompt, sans-serif"
  },
  divOurProject: {
    marginBottom: 40,
    paddingTop: 11
  },
  containerProject: {
    backgroundColor: "#7777777"
  },
  gridProject: {
    backgroundColor: "#FFFFFF",
    paddingBottom: 30
  },
  nameAppTextInHeader: {
    fontSize: "min(max(1vw,5px),3vw)",
    color: "#FFFFFF",
    fontFamily: "Prompt, sans-serif",
    fontStyle: "italic",
    fontWeight: "bold",
    marginBottom: 0
  },
  descriptionInHeader: {
    fontSize: "min(max(0.6vw,5px),1vw)",
    color: "#FFFFFF",
    fontWeight: "normal",
    marginTop: 0,
    fontFamily: "Prompt, sans-serif",
    fontStyle: "normal"
  },
  imgInHeader: {
    maxWidth: 62,
    maxHeight: 62,
    width: "11vw",
    height: "11vw",
    borderRadius: 5
  },
  gridInHeader: {
    marginTop: 14,
    marginBottom: "min(max(4vw,17px),56px)"
  },
  HeaderText: {
    color: "#F6F6F6",
    fontSize: "min(max(2vw,16px),3vw)",
    letterSpacing: 0.07,
    fontFamily: "Prompt, sans-serif",
    marginBottom: 30
  },
  gridTextInHeader: {
    marginBottom: 8
  },
  gridHeaderText: {
    textAlign: "start",
    width: "2.5vw"
  },
  gridHeader: {
    marginTop: 66
  },
  containerHeader: {
    paddingBottom: 30
  },
  gridBreadcrumbs: {
    marginTop: 27,
    backgroundColor: "#000000"
  },
  containerBreadcrumbs: {
    width: "100%",
    marginBottom: 45
  },
  divBody: {
    flex: 1,
    backgroundColor: "#000000"
  },
  linkStyle: {
    color: "#D5D5D5",
    fontFamily: "Prompt, sans-serif",
  },
  linkStyleProject: {
    color: "#FFFFFF",
    fontSize: 13,
    fontFamily: "Prompt, sans-serif"
  },
  lineBottonOurProject: {
    width: 46,
    height: 2,
    backgroundColor: "#329DAF"
  },
  gridButton: {
    width: "100%"
  },
  breadcrumbsStyle: {
    color: "#FFFFFF",
    fontSize: 13
  },
  onLess: {
    textAlign: "end",
    height: "3vw",
    marginRight: "2vw",
    marginBottom: "min(max(0.1vw, 11px), 3.5vw)"
  },
  onMoreLessText: {
    fontSize: "0.7vw",
    fontFamily: "Prompt, sans-serif"
  },
  onMoreLessButton: {
    maxWidth: "17vw",
    maxHeight: "5vw",
  },
  seeMore: {
    textAlign: "end",
    height: "3vw",
    marginBottom: "min(max(0.1vw, 11px), 3.5vw)"
  },
  GirdCardList: {
    marginLeft: "5vw",
    marginRight: "5vw"
  },
  imgCardList: {
    width: "100%"
  },
  cardActionAreaStyle : {
    padding: 12, 
    maxHeight: 304
  }
}));
