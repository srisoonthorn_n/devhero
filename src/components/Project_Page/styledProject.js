import styled from 'styled-components';


export const StyledProject = styled.div`


.slick-next {
    right: -25px;
}

.slick-prev {
    left: -25px;
}

.slick-track {
    width: 1000% !important;
    margin-left : 7px ;
}
.slick-prev:before, .slick-next:before {
    color : #5B5B5B;
    font-size: 24px;
}

`



// .slick-slide {

//     float: left;
//     height: 100%;
//     min-height: 1px;
//     width: 245px !important;
//     margin: 8px;
// }

// .slick-prev:before, .slick-next:before {
//     color : #5B5B5B;
//     font-size: 24px;
// }


// .slick-track {
//     width: 1000% !important;
// }
