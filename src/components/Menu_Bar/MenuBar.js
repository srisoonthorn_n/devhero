import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

function MenuBar() {
    const classes = useStyles();
    return (
        <AppBar position="static" className={classes.appbarStyle}>
            <Toolbar>
                <img src={require('../../images/logo.jpg')} className={classes.imageLogo} />
                <Typography variant="h6" className={classes.title}>
                    DEVHERO
                </Typography>
                <IconButton edge="start" color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton>
            </Toolbar>
        </AppBar>
    );
}

export default MenuBar;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
            fontSize: 24,
            textShadow: '0 0 20px #60D7FF',
            fontFamily: "JackInput"
        },
        appbarStyle: {
            backgroundColor: "#000000",
            height: 70,
        },
        imageLogo: {
            width: 63,
            height: 55,
            marginRight: 10
        }
    }),
);
