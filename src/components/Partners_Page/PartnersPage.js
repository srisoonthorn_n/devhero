import React from 'react';
import MenuBar from '../Menu_Bar/MenuBar';
import { Breadcrumbs, Link, Typography, Container, Paper, Grid, Chip, makeStyles, Fab } from '@material-ui/core';
import ViewCompactIcon from '@material-ui/icons/ViewCompact';
import CopyRight from '../Copy_Right/CopyRight';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import img1 from '../../images/Er/login.png';
import img2 from '../../images/Er/information.png';
import img3 from '../../images/Er/profile.png';
import img4 from '../../images/Er/checklist3.png';
import img5 from '../../images/Er/checklist3.png';
import img6 from '../../images/Er/checklist3.png';
import img7 from '../../images/Er/checklist3.png';
import { ArrowSlider } from './styled';


function PartnerPage() {

    const classes = useStyles();
    var settings = {
        dots: false,
        infinite: false,
        slidesToShow: 4,
        focusOnSelect: true,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 380,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }

        ]
    };

    return (
        <div className={classes.divbody}>
            <MenuBar />
            <div className={classes.divbreadcrumbsStyle}>
                <Container className={classes.containerBreadcrumbs}>
                    <Breadcrumbs className={classes.breadcrumbsStyle} aria-label="breadcrumb">
                        <Link className={classes.linkBreadcrumbs} href="/">
                            Home
                        </Link>
                        <Link className={classes.linkBreadcrumbs} href="/project">
                            Projects
                        </Link>
                        <Typography className={classes.linkBreadcrumbsApp} >Speedy ER</Typography>
                    </Breadcrumbs>
                </Container>
            </div>


            <Grid item xs={12} className={classes.gridHeader}>
                <Container>
                    <Grid
                        item xs={8}
                        lg={12}
                        md={9}
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid
                            item xs={11}
                            sm={10}
                            md={7}
                            lg={6}
                            container
                            direction="column"
                            justify="center"
                            alignItems="flex-start"
                        >

                            <Grid>
                                <h1 className={classes.h1nameApp}>Speedy Er Mobile App</h1>
                            </Grid>
                            <Grid>
                                <p className={classes.pTypeApp}>health care</p>
                            </Grid>
                            <Grid item xs={11} lg={10} md={8}>
                                <p className={classes.pDescriptionApp}>
                                    Contrary to popular belief, Lorem Ipsum is not simply random text.
                                    you has roots in a piece of classical Latin literature from 45 BCdddd
                                    It has roots in a piece of classical Latin literature from 45 BCdddd
                                    </p>
                            </Grid>
                            <Grid container alignItems="flex-start" item xs={12} direction="row">
                                <Grid item xs={6} md={5} sm={5} lg={3}>
                                    <img src={require('../../images/appstore.png')} className={classes.AppStoreStyle} />
                                </Grid>
                                <Grid item xs={5} md={3} sm={4} lg={2}>
                                    <img src={"https://seeklogo.net/wp-content/uploads/2015/07/get-it-on-google-play-vector-400x400.png"} className={classes.playStore} />
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item xs={1}>
                            <img src={require('../../images/partner.png')} className={classes.imgPhone} />
                        </Grid>

                    </Grid>
                </Container>
            </Grid>

            <Grid item xs={12} lg={12} className={classes.gridBody}>

                <Paper className={classes.paperStyle}>
                    <Grid container
                        className={classes.gridInfoPaper}
                        direction="row"
                        justify="center"
                        alignItems="center">
                        <Grid className={classes.gridLineInfo} />
                        <Grid className={classes.gridInfoText}>
                            <h1 className={classes.infoText}>Information</h1>
                        </Grid>
                        <Grid className={classes.gridLineInfo} />
                    </Grid>

                    <Container className={classes.containerAvailable}>

                        <Grid
                            container
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            <Grid container direction="column" item xs={7}>
                                <Grid item xs={9}>
                                    <h1 className={classes.textHeader}>Available on</h1>
                                </Grid>
                                <Grid container direction="row" item xs={4}>
                                    <Grid className={classes.gridChip}>
                                        <Chip className={classes.styleChip} label="PC" />
                                    </Grid>

                                    <Grid className={classes.gridChip}>
                                        <Chip className={classes.styleChip} label="Phone" />
                                    </Grid>

                                    <Grid className={classes.gridChip}>
                                        <Chip className={classes.styleChip} label="IPad" />
                                    </Grid>

                                </Grid>

                            </Grid>

                            <Grid item xs={5}>
                                <Grid className={classes.gridIconApp}>
                                    <img src={require('../../images/logo_er.PNG')} className={classes.imgIconApp} />
                                </Grid>

                            </Grid>
                        </Grid>
                        <Grid>
                            <Grid>
                                <h1 className={classes.textHeader}>Description</h1>
                            </Grid>

                            <Grid>
                                <p className={classes.textDescription}>
                                    The Amazon App for Windows 10 PC/Tablet allows you to shop just as you do on the web.
                                    Search for products using the Amazon App, and easily add them to your shopping cart to purchase.
                                    You have full access to your existing cart and wish lists.
                                </p>
                            </Grid>

                            <Grid>
                                <p className={classes.textDescription}>
                                    คุณต้องเชื่อมโยงโทรศัพท์ Android กับพีซีผ่านทางแอปโทรศัพท์ของคุณ และโทรศัพท์ของคุณต้องใช้ Android 7.0 หรือเวอร์ชันที่ใหม่กว่า
                                    แจ้งให้เราทราบว่าฟีเจอร์ใดที่คุณต้องการเห็นถัดไป โดยเลือก "ส่งคำติชม" ในการตั้งค่าในแอปโทรศัพท์ของคุณ

                                </p>
                            </Grid>

                            <Grid>
                                <p className={classes.textDescription}>
                                    คุณต้องเชื่อมโยงโทรศัพท์ Android กับพีซีผ่านทางแอปโทรศัพท์ของคุณ และโทรศัพท์ของคุณต้องใช้ Android 7.0 หรือเวอร์ชันที่ใหม่กว่า
                                    แจ้งให้เราทราบว่าฟีเจอร์ใดที่คุณต้องการเห็นถัดไป โดยเลือก "ส่งคำติชม" ในการตั้งค่าในแอปโทรศัพท์ของคุณ
                                </p>
                            </Grid>
                        </Grid>

                        <Grid className={classes.gridGallery}>
                            <Grid>
                                <h1 className={classes.textHeader}>Gallery</h1>
                            </Grid>


                            <Grid item xs={12} lg={12} md={12} className={classes.gridSlider} >
                                <ArrowSlider>
                                    <Slider {...settings}>
                                        {
                                            [img1, img2, img3, img4, img5, img6, img7].map((item) => (

                                                <Grid>
                                                    <img src={item} className={classes.imgSlider} />
                                                </Grid>
                                            ))
                                        }
                                    </Slider>
                                </ArrowSlider>
                            </Grid>
                        </Grid>

                        <Grid className={classes.gridPartner}>
                            <Grid>
                                <h1 className={classes.textHeader}>Partner</h1>
                            </Grid>

                            <Grid item={12} container direction="row">
                                <Grid item xs={3} md={2} lg={2} className={classes.gridImgPartner}>
                                    <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAmVBMVEV/ABn///91AAB4AAB+ABb79/h+ABR7AAB3AADr296GHyyMLzrCnqF8AAq2hYt9AA+jYGiXRlDawsV+AA2wfIH27e+qb3bQsbXv5ObUub2cVFxsAACeWWHhzM+SPEeLLDi8j5SueH7Lqa2nanDl0tVpAACygYXDm6CJIzGCDyDt3+HeyMvz6erDnaG6jJHWtruQN0GRQEmVSlL3ksxHAAAJUUlEQVR4nO2caXuiOhSAIQESdRDXKlUrLq21Wq/2//+4SxZIAsG20xl0eM77oY8slrwsSU5y0HEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAMgSX8D7bIbBtyL8VmeuH1sMWdor4ytB2mJ9Ct36RMdZ3wJPSDm6HHRvH5spAKg57rrH+hVgOS7rmTnOmSHbmyvYfUUQtt0RH/8/orWIHGpsrI2kY9sz1VYYGc3al8S9z5d8zfNAvIi5v//cN15ph0Gmi4QJpRVk10XBM1XY8aaKhu1f/Gm0badgL1Xa/FsN+VK/hLq9qgrZlcwMMB/mDGG6aaTjNK1P83kxDl2SdTHRoqOFHlG0eN9TwJEsVOLatTTCMZVUTPTbVsCWrGpI01dCV15AOGmv4JKoaW3D4TxuqzyNeLI/kK6bNMPwv74SKEDF6UpuaYRjnC1te1ZCXfNPptw2NEYObG+bBoM+rGhUc+g+/bxhaDnszQ3XcV/bfVXC4bYjhoJ9/3LByqeBw3RRDlH9kIWLwmi++rBpi+HzMPh7SBxGrC3exGBZGcP4Nw1keLB3TyhSv8y141BRDFfB6noMW2cJ01hRDpAYt5kMtOBygxhiqgacTCfb5wq4xhpTmly2mWlF7tDGGqo1/Q3iXb9iTxhhq1SdVweEYNchwpQ6K8ohp0SDD4JwvjFRwuMbNMXSw6opeVCGbZKiWt0rqNWiQoSq3n1c6PnIaZKgdOm8aWz81JKUckhsaRn23xAT/zBAnE0V8CW5rqA2v5XTJzwzNEUs+/XpDQwdN3SKX6IeGxkz5/NaGlmHu0GuUYXmqgs2XNslwOC8acu8GGXpe0TDBzTIsT/uyXJcvGDqyPMXkhjs0XJjH4flDFsNitlRfzoxrYSVnc3eGxfSLMZvCsBkWTGIxq+oV74HHyDxnN28PS3fZospwZe7nPqB0dTQrJsK1AzOhI+E3SmGvc52G2kg35xe2GwbLgoo7eaW0X8yDS28Bc3Tcb1OCLoW9eCZdXYZaiMjhMxgWQ1smiiUJjvXmC03sbrUu7OTzO7w2w8LEN5+FshlaEzNLpCco+vhsp0W9htg4w2Im0WYYnd3P4fUUtSU46oh54toMzbRgkXhiM3SQPVPDoEtK58yCyDKrzVCbvXd5cFhlaE/NNHjj50cbPbfyS0yE12ZohogiK8NqqE+/VbAU3QBszzyS+DJRsDZDM4uGt1QVhg6yJmcq5lmWgjWZOuNDjuTUZ2i0X+L8Vhh+otjL0zA8XA6sM/JkjfoM9S6VTKatMnRQ9Y06ftLf2yBVV3GTJyTXZ6i3XzJLsdLQwW1rki0bKDfe6QrQzrbXW0edhr9kqD9zB6HjhWpVIjKatNyhgmFa9L7FcdKmXuFQ9LXUuoxXKFI7FA1f/4ihFwwV2etnQZStibICqFXpykLZI+ScFlqjPh48YGopXYD2O+2WGcc9ZGaE6QcpH+a3FTUsK6/vlxeNILzsdZP3XTJ63COKKwoXYBQ8vryvJ+/JZklpaTTc864e5sYEIR/aJtH1e8uLxG7hH7kFAaC5NP4RIWdkm/P7/v9Rb2nfF+HFbW2QLbO3wCeXGh/ZC9mJLQn6tgSEjb8cV59dRkLP18+CyD6yGqIitV5pGejMZ1dK4YWUjo5uj4ZEIxMOBGKkKsFiSW+/6aG1NXj7qFExG8k8jgv4c1GKIaEo2IgAa9Vb6QiL/bnDaXPD3StfOLc1xfIo3dz+ywt/RbB6SEW8Oxv2ktj+nonrOswiKk1ecSbaLX1LwyuC0tAsnTmIJgztb4Ot78MQx7bCmYYqWB/vCkOr3zIc9zacuE7DUAXsI5bxPWFHn/KIPx515Ui3NHxb9xG+Yqg9wXbDKQoZYhykvmt4zq4QL/uKyW35W5YnROQgKd+jFyHWlgvD1Xz+6BcMx3lVTJ+3FYbikPUaOnj2vk0KhnxSuIsI0e7SvXy3jRteSCierG/dpdNnfgJmu3qv4SrpvvCxhhO7S2P28ciNB6ckWeHcUA6hCMN+JFv3bxn62xZje6zV8OqwpnjdSxgScQfOfmBoUJ9hcYbbYKEMp5zjy7bK0O89ZPTe7snw6jXUDCW7asN1nsm2HpcMS3NR9T2Hu0EsXkKMWSG27KPPn8tWHA926jmUJJWGJaShF6XgblKgE6arawlLMaUz/jKlVpfy0dMuopSX8YvXsMLQ2z8xOksBbctPZ762FsWIoGJrkbWH4kYShrwabL2Nvm444c1faEwC+DPjqdiiayX7U4If3RHvRln6NHqv7fO6NI7Fe3DxYCC6gjxQNBM9xjOjZlvUYVhMktERY/5fbA/TPo2IMd5nz8KQBxf3bTj4liHNKuZHEa6IW/C+Db93DamZmDMW3VphmDjt4VgZ9vf7ZX2Gq+3h6G4Hh8XhMBgsFov0b/p5cHQHi51WlxrP4SX9UDb0wpm6YGNH/jKiMMRiiEMaPoVRuzZDByPWHK89tDwdpjM0u+zeOrMobRLfn0WDZqtL096lWzSchb04b9ingRyHkYZEXPTMMApqNHQQrxdafJJ0xAu7ECkXG6IZSkR72MnqfG4Yij6NeMLkjOGxJ0dg78CQiPzXzYyd/9Yzv/mWYnDKMSJgjujTPMkJYvZLdgEy0nG8bFSkNecBpjDsztCzZviKEKnNMNrzO2uLRH4s4f2bWDxlB1QyFNfwIn+J6ISd4MnsVHdQ/nMTx5ODpaEvA39pmI0D1GHoOaL8y0ikymz4ufWReM3kMbQb9kW7sEPsH+h+/mTv0Q/l3CU3by3kS+knKtOY1jP+IM3FEBxPcEK8q9IeikFf0VqQy8PDZi+q2vyNfnewwWwuPyRZhp9PvJsbOvjDZy88OzLCWfNkkukGE1bBiiTQdZyGGfuAz0Nn7WEUhnJOV76u4U9SvVAMAnuoLwasHsgdtPhOiLctxEqGd90nRL325CFK60Hc/c+RyQdp+EGzPAuRyaEPyadPrn9IzijT40TocSFyAEXPe/10ObMTKHvevcuF17+19LxZMoHMgcAk4ouiqMT6u8UBi3oue33Nsk8RKc1CDNFyzX6ZyWtf0m+0wyjk8dKww76fxozBpbbo6ZsELKAtrKnYU8wk8gjYk98M1PejuiJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4N/mf2t87XfYQK2YAAAAAElFTkSuQmCC"}
                                        className={classes.imgPartner} />
                                </Grid>

                                <Grid item xs={3} lg={2} className={classes.gridImgPartner}>
                                    <img src={"https://tcbl-group.com/wp-content/uploads/2019/03/makro-1.png"}
                                        className={classes.imgPartner} />
                                </Grid>

                                <Grid item xs={3} lg={2} className={classes.gridImgPartner}>
                                    <img src={"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxESEhUSEhIWFRUVFxgXGBgYGBUYGhgWGBUXFhcYFxYaHSggGB0lHRYVITEhJSkrLi4uGB8zODMsNygtMCsBCgoKDg0OGxAQGy4lICYtLS8vMC0tLS0tLS0tLS0tLS0tLS0tLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAwADAQEAAAAAAAAAAAAABQYHAQMEAgj/xABMEAABAwIBBwgGBwUGBAcAAAABAAIDBBEGBRIhMUFRYQcTInGBkaGxMkJScsHRFCM0YnOCkkNTorLCFjPS4fDxFRdjoyQ1VGST0+L/xAAbAQEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EADURAAICAAMFBgUEAgIDAAAAAAABAgMEESEFEjFBURMiMmFxsTOBkaHRFUJSwRTwI+FDU/H/2gAMAwEAAhEDEQA/ANxQBAEAQBAEAQBAEB467KsEP95I1p3X0/pGlYTsjDxMjnbCHiZA1eNohojjc/ibNHxPgqssbBeFZlWWOh+1ZkTPjKod6LWM7C495PwUEsbN8EiF42b4JHikxHVu1zEdQaPIKF4m18zD/IsfM6jlioP7eT9TlG77f5M87azqfTcrVH76T9bvmse3t/kzJWz6s748vVQ1TO7c0+YWSxVy/cZq+xcz2wYsqB6WY7rFvIqSOPtXHJkscTLmStLjBh/vIy3i0h3gbKxDaMf3L+yWOJXNEzRZWgl9CQE7joPcVcrxFdnhZNGyMuDPcpjMIAgCAIAgCAIAgCAIAgCAIAgCAICFyxiWCnu0nPf7DdJHvHU3z4KCy+EPUrXYqurR6voU3KeK6mW4a7m27ma+12vusqVmJnLRaGtsxtk9Fp6ERBC+R1mNc9x2AFx6zZV1FyempBFSm9NSdosH1L9Ls2MfeNz3N+anjg7Hx0LUMFY+OhNU2CIx6cr3e6A0eN1PHAx5stRwMVxZIxYUpB+zJ63O+BUqwlS5EywtS5HobkClH7BndfzWf+PV/FGfYV9Dk5Bpf3LO5ef41X8UOxr6HTJhmlP7O3U5w+KxeDpfI8eHr6HinwdEfQe9vXZw+B8VDLZ8HwbRg8NHkyLqsJzt0sLXj9J7jo8VUns+xeF5kbw8lwIeoppIzaRjmniCO47VSnXOD7yyImnHie/J+XZ4tAdnN9l2nuOsKarGW18815kkLZRLRkzEcUtmu6Dtx1Hqd87LaUY6uzR6PzLMbYyJpXSUIAgCAIAgCAIAgCAIAgCA89bWxwsL5HBrRtPkBtPBYykorNmE5xhHek9Cg5dxdJLdkN42b/XcOv1RwHeqF2JlLSOiNRfjpT0hovuQdBQSzOzYmFx27hxcToCrwhKbyRVrqlY8ooueScEsbZ07s8+y24aOs6z4K7XhEtZ6mzq2fFazeflyLTTUrIxmsY1o3AAK3GKiskX4xUVlFZHcvTIIAgCAIAgCAID4lia4WcAQdhFx3LxpNZM8azIHKOFo36YjzZ3a292sdioXbPhLwaexDKiL4aFXr8nywm0jbbjraeo/Baq7DzqfeRXlCUeJ7ck5ekhs09Nm46x7p+Clw+NnVo9UZwtcS50FdHM3OY6+8bQdxGxbuq6Fsd6LLUZKSzR6VKZBAEAQBAEAQBAEAQEblvLMdMzOebk+i0a3H4DeVHZYoLNkN98aY5yMyyvlaWpfnyHV6LR6LRwHxWtsslN5s0N18rXnImsO4SfNaSa7I9YGpzh/SPHzU1WGctZcC1hsC596ei+5fqOjjiaGRtDWjYPM7zxV+MVFZI28IRgsorI716ZhAEAQBAEAQBAEAQBAEB8SxNcC1wBB1g6QvJRUlkzxrPiVTLOGS274NI2s2j3Tt6lqMTs/LvV/T8FedPOJBUlU+J2cw2cPHgQtdXZOqW9HiRRk4vQvGRssMnHsvGtvxG8Lf4bFRuXR9C3CakSatGYQBAEAQBAEAQEbl3K7KaPPdpJ0NbtcfgN5Udligs2Q33xpjvMy6vrZJ5DJIc5ztm4bGtG5ayUnN5s5+yyVst6XEuWFsKBlpqgXfraw6m8Xb3cNnlcpw+73pG0wmC3e/Zx6dC4q2bIIAgCAIAgCAIAgCAIAgCAIAgCAg8vZBEt3x2Eng7r48VQxeDVvej4vchsq3tVxKex7433F2vae0FaROdc+jRWWaZeMh5WbO3ToePSHxHBdBhcSro+ZcrnvIlFaMwgCAIAgCA89fWMhjdI82a0XPwA3k6l5KSis2YTnGEXKXAybLOVX1Mpkfo2Nbsa3YPmVqrLHN5s52+92y3mXDBuHMwComHTOljT6o9o/ePgreHpy70uJs8FhN1dpPjy8i4q2bIIAgCAIAgCAIAgCAIAgCAIAgCAIAgIPEWRRMM9g+sA/UNx47iqOMwnarejx9yK2ve1XEqFLUPieHN0Oaf8AcELSQnKqe8uKK0ZOLNAyZXNmjD29RG47QujoujbBSRcjJSWZ61MZBAEAQBAZrjnLfPS8yw/VxnT95+o9g1dd1QxFm891cjR4/Eb8txcF7n1gfIXPP56QfVsOgH1nj4Dz7Uw9W8958D3A4bflvy4L7s0hXzdhAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAVTFmSbfXsHvjyd8/91qdoYb/yR+f5K11f7kReQMp8xJp9B2h3Dc7s+ap4PEdjPXg+P5MK57rL8CuiLhygCAICDxflb6PAS02kf0GcCdbuwaeuyiunuR0KuMv7KttcXwMyyXROnlZEzW469w1lx6hcrXwi5SyNDVW7JqK5mxUNIyKNsbBZrRYfM8VtIpRWSOmhBQiox4I716ZBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAfMjAQQRcEWI4LxpNZMGd5YoDBKWerrad7T8tXYucxNHZTa5cijOO68i0YSyhnx8249KPVxbs7tXctps+7fhuvivYsUzzWRPLYEwQBAZTjjKfPVLmg9GLoD3r9M9+j8qoXz3pZdDn8fd2luS4LQsXJzkvNY6ocNL+izgwHSe0j+FS4aGS3i7s6nKPaPnw9C6K0bMICPyxlmClZnzPDRsGtzjua0aSvHJLiRW3wqWc2ULKfKZITanha0e1IS4n8rSAO8qF3dDU2bVb+HH6kO7H2UCb860cBGy3iLrDtZFd7Sv6r6HvoeUmqafrY45Bwux3fpHgslc+ZLDati8ST+xeMP4spqvosdmyfu36Hdmx3YpYzUjaYfGV3cHr0J5ZloICp5fx5TU5LGfXSDWGkBoO5z/ldRysSKF+0K6tFqyo1XKPWOJzGxRj3S49pJse5Ru5multS1vupI6YuUOvGsxu62f4SF52sjFbTvXT6FhyRylRuIbURGP77CXN7W+kOy6zVq5lyrakXpNZF5palkjQ+Nwc12kFpBB7VMnnwNpGSks0zuQyKdlLlCghlfE6GUmNxaSMyxINtF3KJ2pPI11m0q65OLT0PrJOP4KiZkLYpQ55sCcyw0E6bO4IrU3ke1bRrsmoJPUt6lNgEBB4nxNHQ82ZGPdzmdbNzdGba97ke0sJTUSricVGjLeWeZA/8zqb9xN/2/wDEsO2XQq/qtf8AFlvyVXCeFkzQQJGhwBtcA77KVPNZmxqsVkFJcyPxXQc5FngdKPpfl9YfHsVPHU9pXmuKML4b0cyp5GreZla/ZezvdOv59i1GGt7KxMrVz3ZJmjBdIXzlAeHLlfzEEku1rSR7x0NHeQsZy3YtkV9nZ1uXQxqlhdK9rBpc9waOtxtc961qW88jmYxc5Jc2/c2yipmxRtjb6LAGjqAstmlksjqYRUYqK5HevTIhsU5eZRwmRwznHosb7TvgBrJWMpbqzK+JxCohvP5GLZSyhLUSGWVxc49wGwNGwDcqjbfE5m22Vkt6TPKvCM7WU7yLhjiN4aSO+y9yZmoSazSf0OpeGB9MeQQQSCDcEGxB3g7F6eptPNGu8n+IpKqJzZWkvjsOct0XjidWeNo7VZrnvLU6HAYmV0GpLVc+v/ZXcfYvc9zqandZjdEjwdLjtaDsaNR39WvCyzkinj8a23XW9ObKEoDUnLWkmwFzuGk9yHqWeiPuWFzfSa5vWCPNenrjKPFHWvDEnMLYjlo5AW3dG4jPj37Lt3O89SzhNxZbwuKlTLTVPl+DbIZM5odpFwDpBB0i+kHUeCtnTJ5rMw3Ff22o/Ff5qnPxM5fF/Hn6nbgr7dT+/wD0le1+JHuC+PH1/o3EK2dQEBnfK9qpuuTyYoLuRqNrcIerM3UBpDcsGfYaf8Nqtw8KOpwnwI+hMuFxYrMsmaZUpeamfHsadHunS3wIXN31dnY4munHdk0XfDVXzkDL629A/l1eFlu8JZv1J/Iu1S3okqrJIUvlOrM2GOIftH3PusHzLe5V8Q+7kazadmVaj1fsQHJ5R85V551RNLvzHot83HsUOHjnLPoU9nQ3rs+iNTV434QGLY9ysaireAehETG0dR6Z7XX7AFVslmzmsfd2lrXJafkr0cZcQ1oJJIAA1kk2ACwKaTbyRreFMEQ07Q+dokmOnTpazg0aifvHssrMK0uJ0OFwEKlvT1l7FuDbKQ2BDZewxTVbTnsAfskaAHA9frDgVjKKkVr8LXcu8tepUMicm55wmqeCxpIDWE3eNhcfVHAaeIUUaupr6dl97Ox6e5ZMXVraGidzIDCbRxhoAsXbQOADj2KSb3Y6F3F2Kih7unJGMKoc0WHB+GXVshuS2Jls9w1knU1vHyUkIbzLeEwjvlrwXE13JeR4KdubDG1g3jWeLnHST1qyklwOhqphWsoLI9csLXAtc0OB1ggEHsK9JGk9GULE/J8HkPow1hJAcwmzLH1m+zbd3cYZ1fxNVidmqT3qtPLkTeGMHQUlnn6yb2yPR9xvq9etZQrUSzhsFCnXi+v4LIpC6YViv7bUfiv81Tn4mcti/jz9TtwV9up/f/pK9r8SPcF8ePr/AEbiFbOoCAzvle1U3XJ5MUF3I0+1uEPVmbqA0puWDPsNP+G1W4eFHU4T4EPQmlmWSnY4prPZIPWBaesaR4E9y1O0YaqfyKmJWTTPrA1T0pI94Dh2aD5hNmz1lH5jDS4ot62xbMu5SqnOqms2MjHe4knwDVTxD72RodpzztS6L3JvkvprRSye08N7Gtv5uKkw67rZb2XHKDl1fsXZWDZnXUSZrHO3AnuF0PJPJZn54LidJ1nSes6SqJx7ebzLTyaUYkrQT+zY6Qdd2sH89+xS1LORf2bBSv15LP8Ao2JWToggCAIDOeV2c/8Ah2bDzjj1jMA/mKgufBGn2tLSK9TOVAaU23AtEIqGG2t7ecJ3l/S8AQOxW4LKKOnwUFCiOXTP6kzVz82xzyCQ0EkDXYJbPcg5dC7CG/JRXMhP7WRexJ/D81rf1aro/t+TYfpdvVff8D+1kXsSfw/NP1aro/t+R+l29V9/wP7VxexJ/D80/Vquj+35H6Xb1X3/AAd1JiSOR7WBjwXG1zm281JVtKuyagk9SO3Z9lcHJtaGSYr+21H4r/NTT8TOIxfx5+p0ZBrxT1EUzgXBjrkC1zoI0X615F5PMwosVdim+Ro9JylUriA+OWMbyGuA68038FOrUbmO1Km8mmi30dXHKwSRvD2nUQbhSp58DYwnGazi80ULle1U3XJ5MUF3I1O1uEPVmbqA0puWDPsNP+G1W4eFHU4T4EPQmlmWSDxjBnU5PsOa7xzT/MqeOjnS30IMQs4FXwvNm1Mf3rt7wbeNlrcHLduX0KtEspo0Nb02ORjmNJc6un4ODR+VjR53VK3WbOaxss75f7yNC5P4s2hj+8Xu73n5BWafAjc4CO7Qvn7liUpcPidmc0t3gjvFkPGs1kfnh8ZaS062ktPWDYqjlkchJZPJll5Oa4RVrc7QJWuivxJa5veWAdqkqeUi5s6xQvWfPQ2VWjpAgCAIDOuV2A2p5NgMjD1uDXD+VyguXBmn2tHSMvUzhQGlNqwDXCWiisdMY5pw3Fmgd7c09qt1vOJ0+BsU6I+Wn0LBIwEEHSDoPUsmk1ky3nlqigZXyY6B5BHQJ6Lt43HiuVxeFlRPy5M6TC4mN0fPmjwqqWggJHDsedUR8CSexp/yVzZ8d7ERKmPlu0SKPiv7bUfiv81vZ+JnzPF/Hn6kSsSsEBZsA5cdT1LWE/VTENcNgcdDXjcb2B4HgFJXLJ5F7AYh12qL4MsXK96NN1yeTFndyLm1uEPVmbqA0puWDPsNP+G1W4eFHU4T4EPQmlmWTw5cjzqeUfcce4X+CivWdcl5GFizg0Z1k2TNljdue0/xBaKp5Ti/NGtg8pJmorojbGJYkderqD/1X+DiPgqFnjZyuJf/ADTfmavg9tqKn/Dae/T8Vcr8COgwayoh6ImFmWQUBjnKJkgwVTngdCa7xuzvXHfp/Mqtkcmc5tCjs7d5cH78yrgkaQbEaiNnUsCiafhXH0b2iOrdmPGgSH0X8XeyfDyViFqa1N5hdoxkt215PryLnHlCFwzmysI3hzSO+6kzRslOL1TIHL2NqWnaQx4mk2NYbi/3njQPPgsZWJFW/HVVrR5vyKVkjlAqY5HOm+tje65bqLL/ALs7uB8FCrXnqayraVkZZz1T+3oXDLD4cqUTxTuDnts9rdTmvbpDXDZcXG7SpZZTjobG1wxdDUHm/wCzICFVOdJvCuI5KKTOAzo3WD2b7anN3OGn/WrOE90s4XFSolnxXNGtZKxJS1ABjmbf2XENcOtp092hWVJPgdDViarV3WejKFfTsaeekjDdueW2PYdaTUWspcCR3Rr7zll8zLsUYkhc7No2ZjQdMhB6XBrDoA4kX6tuunhcO3pH3KOI2/iPDTN+rS/sgf8AjFR+8/hZ8lH/AIdP8ff8lX9d2h/7PtH8GhcmdPM9j6iV1w7oR6GjQD0naBvsOwq3hsNXW96KyNlhMZisRW3fLNctEvnoiiYr+21H4r/NJ+JnP4v48/U4wvSslq4Y5G5zHOs4adIsdy9gs5I8wsIzujGXA1b+xWT/AP0zf1Sf4lY7OJv/APBw/wDFH0zBlACCKdoIIIOdJoI1esnZx6HqwVCeaiVrle9Gm65PJiju5FHa3CHqzN1AaU3LBn2Gn/DarcPCjqcJ8CHoTSzLJ01gvG8b2u8isZapnkuDMqidpB4hc7HijTxeqNZuujzNyYhiQWq6gf8AWk8XEqlPxM5bErK6fqzWcIOvRU/4bfDQrdfhR0GD+BD0RMLMshARmIcjR1cJik62u2tcNTh/rSCVjKO8siG+iN0N2Ri2W8jzUsnNytsfVcPReN7Tt6tiqyi09Tmb6J0y3Zf/AEj1iQiyAIDtpqd8jgyNpc5xsGgXJK9WvAyjFye6lmzXsEYVFGwvfYzPFnEamt15jT5nbbgrNcN06LBYTsI5vxMgcfYOcXOqqZt76ZIxrvte0beI7VhZXzRVx+Bz/wCSteqM6UBpRZAAEAQFiwlhaSseHG7YAek/fbW1m88dngpIQci7hMHK95vwmy01O2NjWMAa1oAAGoAaAFaOjjFRWS4GHYr+21H4r/NU5+JnMYv48/U7cFfbqf3/AOkr2vxI9wXx4+v9G4hWzqAgM75XtVN1yeTFBdyNRtbhD1Zm6gNIblgz7DT/AIbVbh4UdThPgQ9CaWZZOqpPQd7p8ljPwv0Mo6yRmrG6R2LjottrU6eVNaT7q+hpll2O6cqYzjOPNrqgffB/UxrviqlmkmczjVlfL/eRo+AJc6hi+7nN7nu/yVmrwI3eAlvUR/3mWJSFwIAgPNlCginYY5WNe07CNu8HYeIXjSfEwnXGa3ZLNFIynyZxk3p5iz7rxnjscLEdt1E6VyNZbsqL8Dy+5Du5Nay+iSEjfd48MxY9iys9lW8mvue+h5MXX+uqBbdG3T+p3yXqp6slhsn+cvoXXIuQKekFoYwCdbjpc7rcdnAaFLGKXA2dOHrpWUESiyJwgK3l7BdLUkvLTHIdb2WFz95up3Xr4rCVakU78DVa83o+qKhV8mdQD9XNG4feDmHwDlF2LNdPZU/2yTOqHk1qyelJC0cC93hmhOxZitlW82ixZH5OaeMh0zjMRs9FncDc9pss41JcS7TsyuGs9fYucUTWgNaA1oFgALADcANSlNikkskfaHpQ8q8nfPTSTfSM3nHl1ubva5va+dpUTqzeeZq7dmqybnvcT6yJyf8A0eeOb6Rnc26+bmWvoI152jWkasnnmKdndlYp73AvSlNoEBXcYYY+nCMc7zfNlx9HOvnZvEW9FYThvFPF4X/IS1yyK1/yv/8Adf8Ab/8A0sOx8yl+kr+X2L1kWh5iCOHOzubaG3ta9tttilislkbWmvs4KHQ9q9JDyZWfmwyH7jvJQ4mW7VJ+TJaFnbFeaKDRMvIwb3tH8QXKUxzsivNHS3Pdrk/Jmj2XZHJ5GU8plPm1gdsfG09rSWnwDVUuXeNFtOOVyfVFi5LKq9PJHtZJfsc0fEOUlD0yLmy551OPRl2UxswgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAiMUzZtO4e0Q3xufAFUNoz3cO/PJFzAQ3r15FYw/FnVEfAl3cCfOy0uAhvYiPlr9Db46e7RLz0L5ZdQc3kUXlWo7xQzD1HFh6ni48W+KhvWmZq9qwzhGfR+5C8mNdmVRjJ0SsIHvM6Q8M9YUvvZFXZlm7a49V7GrK0b8IAgCAIAgCA6m1DC4sD2l7QCWgjOAOolusIBFUMcXNa9riw2cAQS07nAau1AdByrTh/NmeLPvbMz2Z192be6A7qqqjjbnSPaxvtOcGjvOhAfAyhCWc6JY+b9vPbm/qvZAfVNVxyaY5GPtrzXB3kUB1nKlOAHGeLNJIBz2WJGsA31hAcwZSge7NZNG5x1Br2k9wKA7IKuN5c1kjHFuhwa4EtOqzgDo1HuQHLKhhcWB7S5ts5oIJbfVcawgO1AEAQBAEAQBAVXGdRpZH1uPkP6lpNr2eGHzNvsuvxT+R14Ngu97/ZAaOsm/w8VhsivOcp9NDPak8oxh8y22W+NIReKMnfSKWWIC7i27ffb0m+IA7VjNZrIhxNXaVSiYrkytMMsczdbHNdbfY6R2i47VTTyeZzFdjrmp9Gb3TzNe1r2m7XAOB3gi4V462LUlmjsQ9CAIAgCAIDO3V8sOV6x0VM6oJjiBa1waQObj0klAR9JlKVkeWJg0xSF0XRvdzC5z2nSNoBOlATIwnR/8LzuaaX/R+d531s/m8++duvs1WQES6lqqijyfVc19KEAkD4XHS8B5Y11j6Rszib20HSgGIK6lmyTMaWLmbTs5yMi2bIS0HRqtoGrdqBugPZkWmZJlKGaip3wwsjcJnFhja8lrgGhp1m5b3X2IDnAGSoJ6B3PRMkzJpS3OF7EsZe3cEB0YNZT0+S/p5iZz0YlzXkdIuLixovxJA7UB5MIyR0lXSlszJPpcRZNZ7XFsxdntzgDoNy1vWXICy5A/83yh7kP8jUBcUAQBAEAQBAcFAZ7liq52Z79l7DqGgfPtXJ4u3tbpS5cPkjpsJV2dSjzLZhilzIGk639PsOrwAW+2dV2dCz56/g0uOs37n5aEurxTCAxbG+Svo9W8AWZJ9YzqcekOx1+yyp2RykczjqezufR6l15Msr85AYHHpQ6uMbtXcbjuU9Ms1kbTZl+/XuPivYuilNkEAQBAEAQENRZC5utnrOcvzzGtzM22bmhovnX03zd21AeemwswOrDI/PbWEFzbZuaBnanXNz0tejUgIwYKqeb+jHKMn0bVzfNszsz2ecvq7LcLICUrcgTBsTaOqdTNiZzebmNkaWjUSHEdLigI+XArXUklPz7ucmkEskpaCXOvc9C4sNe3aUBbwEBCYYw/9DgdBzmfnOc7Ozc22cALWudyAjGYJtRRURqCWMl5x5zLc43OLsy2d0devTqQHqy5g2nmY0QtZTyNe17ZGRtuC3YQLXG3XrAQHTV4UnNTLUwVphdMGhwETX+i0N1udwv2oCz0zHNY1rnZzg0AusBnECxdYar67IDsQBAEAQBARWI67moTY9J/RHbrPYPgqWPv7Kp5cXoi1g6e1tXRasp2TqQyyNj3nT7o1+C53DU9rYof7kb7EW9lW5/7maKwWAA1BdcuBy5yvQEBVeUPIv0imL2C8kN3ttrLfXb3C/W0KOyO8ihtCjtKs1xWpmWHcrOpZ2TN0gaHD2mH0h8RxAVeMt15mjw9zpsU18/Q3KmnbIxr2HOa4AgjaCLgq4tTqoyUlmuB2oehAEAQBAEAQBAEAQBAEAQBAEAQBAEAQBAcEoChZeyhz0pI9FvRb1bT2/JctjsR21ua4LRfk6PBYfsq9eL4/gm8I0Ga0ykaXaG+7v7T5LZ7Lw+7DtHz9jXbSv3p9muC9yxrbGtCAIAUBjmPMgfRZ85g+qlJc3c12tzPiOB4KrZHdZzmOw3ZWZrg/cmOTbEmafokp0OJMROxx0lnbrHG42hZ1T/ayzs3FZf8Uvl+DS1OboIAgCAIAgCAIAgCAIAgCAIAgCAIAgCAICu4pyrmjmWHpOHS4N3dZ8lqdpYvcXZR4vj6Gy2fht+XaS4Lh6lfyRQGaQN9UaXHcPmdS1OEw7vs3eXM2mKvVNefPkaBGwAAAWAFhwC6tJJZI5ptvVn0vTwIAgCAj8uZKjqoXQyanajta4anDiP8l5KKksmRXUxtg4SMRynk+SmldFILPYdBG0eq5p3FVHFxeRy9tUqpuMuKNQwLioVLOZlP17B/8jR6w47x29Viue9oze4HGdrHdl4l9y3KQ2AQBAEAQBAEAQBAEAQBAEAQBAEAQBAReXMqiBujS93oj4ngqeMxaoj5vgWsLhnfLy5lJAfI/a57z2klc137Z9Wzoe5VDokXrI2ThBGG63HS47z8gunwmGVEN3nzOcxOId097lyJBWiuEAQBAEAQFfxhhplbHos2VnoO/pd90+GvgcJw3kVMXhVfHzXAx6SOWnlsc6OWN3UWkaiD/q6q6pnONTqnlwaNUwbjBlUBFKQ2cDqEnFvHe3u4WYWb3Hib/B42N3dlpL3LapC+EAQBAEAQBAEAQBAEAQBAEAQBAQmIsQx0zbelIR0WfF24eahtuVa8yricVGlefQocWUXzP+sJc950HidTQPJaHFVSslvrV/7wLmxtsLPsL/k/6f8AT+TL5h7I3MjPePrD/CNw471s8DglSt6Xif2LmMxbue7Hwr7k2tiUQgCAIAgCAIAgK9i3C0dY2/oTNHRf/S/e3y7wcJwUipisJG9dH1Mhr6GamkMcjSx7Tcd+hzXDWOIVZpxepztlc6p7slk/94F5wrj+1oqw8BL/APYP6h271LC3kzaYXaX7bfr+TQ4pWuAc0ggi4INwRvBU5uE01mj7Q9CAIAgCAIAgCAIAgCAIDglAVDEWM2svHT2e/UX62t6vaPh1qrbiEtI8TW4nHqPdr1fXkUdokmk9aSR56ySqWs31Zqe9ZLq2aJhfDDacCSSzpfBnBu87z/o36aFDV8Td4XCKrvS1l7FkVguhAEAQBAEAQBAEAQEblzIkFXHmStv7Lhoc072n4aisZRUuJDfRC6O7JGUYkwlUUhLrc5FskaNXvj1evVxVedbiaDE4KynXjHr+TzZBxHUUh+qfdm2N2lh6h6p4jxWMZuJHh8VZT4Xp05Gj5Dx3Sz2bIeZfueeiTwfq77KxG1M3VG0KrNJaPz/JamuBFwbgqQvnKAIAgCAIAgCAIDglAQOV8WU0Fxnc4/2WWNut2oefBRTujEqXY2qvTPN+RRMt4mnqbgnMj9huo+8dbvLgqVl0p+SNRfi7LdHouiOrImQpqk/Vts3a8+iOr2jwHgsa6pT4GNGGnc+7w6mk5CyBDStswXefSedZ4DcOA8VsK6owWhvKMNClacepLKQsBAEAQBAEAQBAEAQBAEBw4X0FAU7L/J/BNd8B5l52AXjJ4t9Xs7lFKpPga7EbNrnrDR/Yz7LGHKqlvzsZzR67ekzvGrtsoJQcTT3YW2rxLTqdeS8uVNP/AHMzmj2dbf0uuF4pyXBmNWItr8EvwW3J3KTILCeFrvvMJaf0m4PeFKr+qNhXtWS8cc/QsFHj6if6Tnxn77D5tuFmrolyG0aJcXl6kvBiGjf6NTEfztHgSs9+PUsRxNUuEl9T1trYjqkYepzfmvc0S78epy6siGuRg/MPmmaG/HqeWbLtIz0qiIfnb5ArxziuZHLEVR4yX1IyqxvRM1SF53Ma4+JsPFYO+C5kE8fRHnn6EHXcohOiGG3GQ/0t+ailieiKtm1P4R+pWco4gqp7iSV2afVb0W9w19t1BK2UuLKFmKts8UjryXkmeoNooy4e1qaOtx0LGNcpcEY1UWW+BfgvGRcCxss6odzh9gXDB1nW7wCtQwyWstTa07OjHWzX2LfFGGgNaAANAAFgBwCsmySSWSPtD0IAgCAIAgCAIAgCAIAgCAIAgOCEBAZUwbRT3JiDHH1o+gesgdE9oWEq4sqW4GmzVrJ+WhV67k1eNMM4I3SAg/qbfyUTo6MoT2U14JfX/ogqrBlfH+xzxvY5rvC4Pgo3VJFWeBvj+3P0IybJVQz06eUdcb/O1lg4NcivKmyPGL+h5jCRrYR1grHIw3HzX2OWwk6mk9iZDc8vseqHJs7vRhlPVG8/BNxvgiRU2S4Rf0ZJ0uE65+qBzRveWt8Cb+CyVM3yJ4YK+X7cvUnKHk8mOmWZrBuYC495sFIsM+bLUNlyfil9Cy5NwZRxWJYZXb5DnD9Po+CmjTCJerwFMOWfqWBrABYAADYFLkXMj6QBAEAQBAEAQBAEAQBAEAQBAEAQBAEAQHBQHCBnKA4cvTFnDEPUcrwyYXhichenpygCAIAgCAIAgCAIAgCA/9k="}
                                        className={classes.imgPartner} />
                                </Grid>
                                <Grid item xs={3} lg={2} className={classes.gridImgPartner}>
                                    <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAmVBMVEV/ABn///91AAB4AAB+ABb79/h+ABR7AAB3AADr296GHyyMLzrCnqF8AAq2hYt9AA+jYGiXRlDawsV+AA2wfIH27e+qb3bQsbXv5ObUub2cVFxsAACeWWHhzM+SPEeLLDi8j5SueH7Lqa2nanDl0tVpAACygYXDm6CJIzGCDyDt3+HeyMvz6erDnaG6jJHWtruQN0GRQEmVSlL3ksxHAAAJUUlEQVR4nO2caXuiOhSAIQESdRDXKlUrLq21Wq/2//+4SxZIAsG20xl0eM77oY8slrwsSU5y0HEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAMgSX8D7bIbBtyL8VmeuH1sMWdor4ytB2mJ9Ct36RMdZ3wJPSDm6HHRvH5spAKg57rrH+hVgOS7rmTnOmSHbmyvYfUUQtt0RH/8/orWIHGpsrI2kY9sz1VYYGc3al8S9z5d8zfNAvIi5v//cN15ph0Gmi4QJpRVk10XBM1XY8aaKhu1f/Gm0badgL1Xa/FsN+VK/hLq9qgrZlcwMMB/mDGG6aaTjNK1P83kxDl2SdTHRoqOFHlG0eN9TwJEsVOLatTTCMZVUTPTbVsCWrGpI01dCV15AOGmv4JKoaW3D4TxuqzyNeLI/kK6bNMPwv74SKEDF6UpuaYRjnC1te1ZCXfNPptw2NEYObG+bBoM+rGhUc+g+/bxhaDnszQ3XcV/bfVXC4bYjhoJ9/3LByqeBw3RRDlH9kIWLwmi++rBpi+HzMPh7SBxGrC3exGBZGcP4Nw1keLB3TyhSv8y141BRDFfB6noMW2cJ01hRDpAYt5kMtOBygxhiqgacTCfb5wq4xhpTmly2mWlF7tDGGqo1/Q3iXb9iTxhhq1SdVweEYNchwpQ6K8ohp0SDD4JwvjFRwuMbNMXSw6opeVCGbZKiWt0rqNWiQoSq3n1c6PnIaZKgdOm8aWz81JKUckhsaRn23xAT/zBAnE0V8CW5rqA2v5XTJzwzNEUs+/XpDQwdN3SKX6IeGxkz5/NaGlmHu0GuUYXmqgs2XNslwOC8acu8GGXpe0TDBzTIsT/uyXJcvGDqyPMXkhjs0XJjH4flDFsNitlRfzoxrYSVnc3eGxfSLMZvCsBkWTGIxq+oV74HHyDxnN28PS3fZospwZe7nPqB0dTQrJsK1AzOhI+E3SmGvc52G2kg35xe2GwbLgoo7eaW0X8yDS28Bc3Tcb1OCLoW9eCZdXYZaiMjhMxgWQ1smiiUJjvXmC03sbrUu7OTzO7w2w8LEN5+FshlaEzNLpCco+vhsp0W9htg4w2Im0WYYnd3P4fUUtSU46oh54toMzbRgkXhiM3SQPVPDoEtK58yCyDKrzVCbvXd5cFhlaE/NNHjj50cbPbfyS0yE12ZohogiK8NqqE+/VbAU3QBszzyS+DJRsDZDM4uGt1QVhg6yJmcq5lmWgjWZOuNDjuTUZ2i0X+L8Vhh+otjL0zA8XA6sM/JkjfoM9S6VTKatMnRQ9Y06ftLf2yBVV3GTJyTXZ6i3XzJLsdLQwW1rki0bKDfe6QrQzrbXW0edhr9kqD9zB6HjhWpVIjKatNyhgmFa9L7FcdKmXuFQ9LXUuoxXKFI7FA1f/4ihFwwV2etnQZStibICqFXpykLZI+ScFlqjPh48YGopXYD2O+2WGcc9ZGaE6QcpH+a3FTUsK6/vlxeNILzsdZP3XTJ63COKKwoXYBQ8vryvJ+/JZklpaTTc864e5sYEIR/aJtH1e8uLxG7hH7kFAaC5NP4RIWdkm/P7/v9Rb2nfF+HFbW2QLbO3wCeXGh/ZC9mJLQn6tgSEjb8cV59dRkLP18+CyD6yGqIitV5pGejMZ1dK4YWUjo5uj4ZEIxMOBGKkKsFiSW+/6aG1NXj7qFExG8k8jgv4c1GKIaEo2IgAa9Vb6QiL/bnDaXPD3StfOLc1xfIo3dz+ywt/RbB6SEW8Oxv2ktj+nonrOswiKk1ecSbaLX1LwyuC0tAsnTmIJgztb4Ot78MQx7bCmYYqWB/vCkOr3zIc9zacuE7DUAXsI5bxPWFHn/KIPx515Ui3NHxb9xG+Yqg9wXbDKQoZYhykvmt4zq4QL/uKyW35W5YnROQgKd+jFyHWlgvD1Xz+6BcMx3lVTJ+3FYbikPUaOnj2vk0KhnxSuIsI0e7SvXy3jRteSCierG/dpdNnfgJmu3qv4SrpvvCxhhO7S2P28ciNB6ckWeHcUA6hCMN+JFv3bxn62xZje6zV8OqwpnjdSxgScQfOfmBoUJ9hcYbbYKEMp5zjy7bK0O89ZPTe7snw6jXUDCW7asN1nsm2HpcMS3NR9T2Hu0EsXkKMWSG27KPPn8tWHA926jmUJJWGJaShF6XgblKgE6arawlLMaUz/jKlVpfy0dMuopSX8YvXsMLQ2z8xOksBbctPZ762FsWIoGJrkbWH4kYShrwabL2Nvm444c1faEwC+DPjqdiiayX7U4If3RHvRln6NHqv7fO6NI7Fe3DxYCC6gjxQNBM9xjOjZlvUYVhMktERY/5fbA/TPo2IMd5nz8KQBxf3bTj4liHNKuZHEa6IW/C+Db93DamZmDMW3VphmDjt4VgZ9vf7ZX2Gq+3h6G4Hh8XhMBgsFov0b/p5cHQHi51WlxrP4SX9UDb0wpm6YGNH/jKiMMRiiEMaPoVRuzZDByPWHK89tDwdpjM0u+zeOrMobRLfn0WDZqtL096lWzSchb04b9ingRyHkYZEXPTMMApqNHQQrxdafJJ0xAu7ECkXG6IZSkR72MnqfG4Yij6NeMLkjOGxJ0dg78CQiPzXzYyd/9Yzv/mWYnDKMSJgjujTPMkJYvZLdgEy0nG8bFSkNecBpjDsztCzZviKEKnNMNrzO2uLRH4s4f2bWDxlB1QyFNfwIn+J6ISd4MnsVHdQ/nMTx5ODpaEvA39pmI0D1GHoOaL8y0ikymz4ufWReM3kMbQb9kW7sEPsH+h+/mTv0Q/l3CU3by3kS+knKtOY1jP+IM3FEBxPcEK8q9IeikFf0VqQy8PDZi+q2vyNfnewwWwuPyRZhp9PvJsbOvjDZy88OzLCWfNkkukGE1bBiiTQdZyGGfuAz0Nn7WEUhnJOV76u4U9SvVAMAnuoLwasHsgdtPhOiLctxEqGd90nRL325CFK60Hc/c+RyQdp+EGzPAuRyaEPyadPrn9IzijT40TocSFyAEXPe/10ObMTKHvevcuF17+19LxZMoHMgcAk4ouiqMT6u8UBi3oue33Nsk8RKc1CDNFyzX6ZyWtf0m+0wyjk8dKww76fxozBpbbo6ZsELKAtrKnYU8wk8gjYk98M1PejuiJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4N/mf2t87XfYQK2YAAAAAElFTkSuQmCC"}
                                        className={classes.imgPartner} />
                                </Grid>
                                <Grid item xs={3} lg={2} className={classes.gridImgPartner}>
                                    <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAmVBMVEV/ABn///91AAB4AAB+ABb79/h+ABR7AAB3AADr296GHyyMLzrCnqF8AAq2hYt9AA+jYGiXRlDawsV+AA2wfIH27e+qb3bQsbXv5ObUub2cVFxsAACeWWHhzM+SPEeLLDi8j5SueH7Lqa2nanDl0tVpAACygYXDm6CJIzGCDyDt3+HeyMvz6erDnaG6jJHWtruQN0GRQEmVSlL3ksxHAAAJUUlEQVR4nO2caXuiOhSAIQESdRDXKlUrLq21Wq/2//+4SxZIAsG20xl0eM77oY8slrwsSU5y0HEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAMgSX8D7bIbBtyL8VmeuH1sMWdor4ytB2mJ9Ct36RMdZ3wJPSDm6HHRvH5spAKg57rrH+hVgOS7rmTnOmSHbmyvYfUUQtt0RH/8/orWIHGpsrI2kY9sz1VYYGc3al8S9z5d8zfNAvIi5v//cN15ph0Gmi4QJpRVk10XBM1XY8aaKhu1f/Gm0badgL1Xa/FsN+VK/hLq9qgrZlcwMMB/mDGG6aaTjNK1P83kxDl2SdTHRoqOFHlG0eN9TwJEsVOLatTTCMZVUTPTbVsCWrGpI01dCV15AOGmv4JKoaW3D4TxuqzyNeLI/kK6bNMPwv74SKEDF6UpuaYRjnC1te1ZCXfNPptw2NEYObG+bBoM+rGhUc+g+/bxhaDnszQ3XcV/bfVXC4bYjhoJ9/3LByqeBw3RRDlH9kIWLwmi++rBpi+HzMPh7SBxGrC3exGBZGcP4Nw1keLB3TyhSv8y141BRDFfB6noMW2cJ01hRDpAYt5kMtOBygxhiqgacTCfb5wq4xhpTmly2mWlF7tDGGqo1/Q3iXb9iTxhhq1SdVweEYNchwpQ6K8ohp0SDD4JwvjFRwuMbNMXSw6opeVCGbZKiWt0rqNWiQoSq3n1c6PnIaZKgdOm8aWz81JKUckhsaRn23xAT/zBAnE0V8CW5rqA2v5XTJzwzNEUs+/XpDQwdN3SKX6IeGxkz5/NaGlmHu0GuUYXmqgs2XNslwOC8acu8GGXpe0TDBzTIsT/uyXJcvGDqyPMXkhjs0XJjH4flDFsNitlRfzoxrYSVnc3eGxfSLMZvCsBkWTGIxq+oV74HHyDxnN28PS3fZospwZe7nPqB0dTQrJsK1AzOhI+E3SmGvc52G2kg35xe2GwbLgoo7eaW0X8yDS28Bc3Tcb1OCLoW9eCZdXYZaiMjhMxgWQ1smiiUJjvXmC03sbrUu7OTzO7w2w8LEN5+FshlaEzNLpCco+vhsp0W9htg4w2Im0WYYnd3P4fUUtSU46oh54toMzbRgkXhiM3SQPVPDoEtK58yCyDKrzVCbvXd5cFhlaE/NNHjj50cbPbfyS0yE12ZohogiK8NqqE+/VbAU3QBszzyS+DJRsDZDM4uGt1QVhg6yJmcq5lmWgjWZOuNDjuTUZ2i0X+L8Vhh+otjL0zA8XA6sM/JkjfoM9S6VTKatMnRQ9Y06ftLf2yBVV3GTJyTXZ6i3XzJLsdLQwW1rki0bKDfe6QrQzrbXW0edhr9kqD9zB6HjhWpVIjKatNyhgmFa9L7FcdKmXuFQ9LXUuoxXKFI7FA1f/4ihFwwV2etnQZStibICqFXpykLZI+ScFlqjPh48YGopXYD2O+2WGcc9ZGaE6QcpH+a3FTUsK6/vlxeNILzsdZP3XTJ63COKKwoXYBQ8vryvJ+/JZklpaTTc864e5sYEIR/aJtH1e8uLxG7hH7kFAaC5NP4RIWdkm/P7/v9Rb2nfF+HFbW2QLbO3wCeXGh/ZC9mJLQn6tgSEjb8cV59dRkLP18+CyD6yGqIitV5pGejMZ1dK4YWUjo5uj4ZEIxMOBGKkKsFiSW+/6aG1NXj7qFExG8k8jgv4c1GKIaEo2IgAa9Vb6QiL/bnDaXPD3StfOLc1xfIo3dz+ywt/RbB6SEW8Oxv2ktj+nonrOswiKk1ecSbaLX1LwyuC0tAsnTmIJgztb4Ot78MQx7bCmYYqWB/vCkOr3zIc9zacuE7DUAXsI5bxPWFHn/KIPx515Ui3NHxb9xG+Yqg9wXbDKQoZYhykvmt4zq4QL/uKyW35W5YnROQgKd+jFyHWlgvD1Xz+6BcMx3lVTJ+3FYbikPUaOnj2vk0KhnxSuIsI0e7SvXy3jRteSCierG/dpdNnfgJmu3qv4SrpvvCxhhO7S2P28ciNB6ckWeHcUA6hCMN+JFv3bxn62xZje6zV8OqwpnjdSxgScQfOfmBoUJ9hcYbbYKEMp5zjy7bK0O89ZPTe7snw6jXUDCW7asN1nsm2HpcMS3NR9T2Hu0EsXkKMWSG27KPPn8tWHA926jmUJJWGJaShF6XgblKgE6arawlLMaUz/jKlVpfy0dMuopSX8YvXsMLQ2z8xOksBbctPZ762FsWIoGJrkbWH4kYShrwabL2Nvm444c1faEwC+DPjqdiiayX7U4If3RHvRln6NHqv7fO6NI7Fe3DxYCC6gjxQNBM9xjOjZlvUYVhMktERY/5fbA/TPo2IMd5nz8KQBxf3bTj4liHNKuZHEa6IW/C+Db93DamZmDMW3VphmDjt4VgZ9vf7ZX2Gq+3h6G4Hh8XhMBgsFov0b/p5cHQHi51WlxrP4SX9UDb0wpm6YGNH/jKiMMRiiEMaPoVRuzZDByPWHK89tDwdpjM0u+zeOrMobRLfn0WDZqtL096lWzSchb04b9ingRyHkYZEXPTMMApqNHQQrxdafJJ0xAu7ECkXG6IZSkR72MnqfG4Yij6NeMLkjOGxJ0dg78CQiPzXzYyd/9Yzv/mWYnDKMSJgjujTPMkJYvZLdgEy0nG8bFSkNecBpjDsztCzZviKEKnNMNrzO2uLRH4s4f2bWDxlB1QyFNfwIn+J6ISd4MnsVHdQ/nMTx5ODpaEvA39pmI0D1GHoOaL8y0ikymz4ufWReM3kMbQb9kW7sEPsH+h+/mTv0Q/l3CU3by3kS+knKtOY1jP+IM3FEBxPcEK8q9IeikFf0VqQy8PDZi+q2vyNfnewwWwuPyRZhp9PvJsbOvjDZy88OzLCWfNkkukGE1bBiiTQdZyGGfuAz0Nn7WEUhnJOV76u4U9SvVAMAnuoLwasHsgdtPhOiLctxEqGd90nRL325CFK60Hc/c+RyQdp+EGzPAuRyaEPyadPrn9IzijT40TocSFyAEXPe/10ObMTKHvevcuF17+19LxZMoHMgcAk4ouiqMT6u8UBi3oue33Nsk8RKc1CDNFyzX6ZyWtf0m+0wyjk8dKww76fxozBpbbo6ZsELKAtrKnYU8wk8gjYk98M1PejuiJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4N/mf2t87XfYQK2YAAAAAElFTkSuQmCC"}
                                        className={classes.imgPartner} />
                                </Grid>
                                <Grid item xs={3} lg={2} className={classes.gridImgPartner}>
                                    <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAmVBMVEV/ABn///91AAB4AAB+ABb79/h+ABR7AAB3AADr296GHyyMLzrCnqF8AAq2hYt9AA+jYGiXRlDawsV+AA2wfIH27e+qb3bQsbXv5ObUub2cVFxsAACeWWHhzM+SPEeLLDi8j5SueH7Lqa2nanDl0tVpAACygYXDm6CJIzGCDyDt3+HeyMvz6erDnaG6jJHWtruQN0GRQEmVSlL3ksxHAAAJUUlEQVR4nO2caXuiOhSAIQESdRDXKlUrLq21Wq/2//+4SxZIAsG20xl0eM77oY8slrwsSU5y0HEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAMgSX8D7bIbBtyL8VmeuH1sMWdor4ytB2mJ9Ct36RMdZ3wJPSDm6HHRvH5spAKg57rrH+hVgOS7rmTnOmSHbmyvYfUUQtt0RH/8/orWIHGpsrI2kY9sz1VYYGc3al8S9z5d8zfNAvIi5v//cN15ph0Gmi4QJpRVk10XBM1XY8aaKhu1f/Gm0badgL1Xa/FsN+VK/hLq9qgrZlcwMMB/mDGG6aaTjNK1P83kxDl2SdTHRoqOFHlG0eN9TwJEsVOLatTTCMZVUTPTbVsCWrGpI01dCV15AOGmv4JKoaW3D4TxuqzyNeLI/kK6bNMPwv74SKEDF6UpuaYRjnC1te1ZCXfNPptw2NEYObG+bBoM+rGhUc+g+/bxhaDnszQ3XcV/bfVXC4bYjhoJ9/3LByqeBw3RRDlH9kIWLwmi++rBpi+HzMPh7SBxGrC3exGBZGcP4Nw1keLB3TyhSv8y141BRDFfB6noMW2cJ01hRDpAYt5kMtOBygxhiqgacTCfb5wq4xhpTmly2mWlF7tDGGqo1/Q3iXb9iTxhhq1SdVweEYNchwpQ6K8ohp0SDD4JwvjFRwuMbNMXSw6opeVCGbZKiWt0rqNWiQoSq3n1c6PnIaZKgdOm8aWz81JKUckhsaRn23xAT/zBAnE0V8CW5rqA2v5XTJzwzNEUs+/XpDQwdN3SKX6IeGxkz5/NaGlmHu0GuUYXmqgs2XNslwOC8acu8GGXpe0TDBzTIsT/uyXJcvGDqyPMXkhjs0XJjH4flDFsNitlRfzoxrYSVnc3eGxfSLMZvCsBkWTGIxq+oV74HHyDxnN28PS3fZospwZe7nPqB0dTQrJsK1AzOhI+E3SmGvc52G2kg35xe2GwbLgoo7eaW0X8yDS28Bc3Tcb1OCLoW9eCZdXYZaiMjhMxgWQ1smiiUJjvXmC03sbrUu7OTzO7w2w8LEN5+FshlaEzNLpCco+vhsp0W9htg4w2Im0WYYnd3P4fUUtSU46oh54toMzbRgkXhiM3SQPVPDoEtK58yCyDKrzVCbvXd5cFhlaE/NNHjj50cbPbfyS0yE12ZohogiK8NqqE+/VbAU3QBszzyS+DJRsDZDM4uGt1QVhg6yJmcq5lmWgjWZOuNDjuTUZ2i0X+L8Vhh+otjL0zA8XA6sM/JkjfoM9S6VTKatMnRQ9Y06ftLf2yBVV3GTJyTXZ6i3XzJLsdLQwW1rki0bKDfe6QrQzrbXW0edhr9kqD9zB6HjhWpVIjKatNyhgmFa9L7FcdKmXuFQ9LXUuoxXKFI7FA1f/4ihFwwV2etnQZStibICqFXpykLZI+ScFlqjPh48YGopXYD2O+2WGcc9ZGaE6QcpH+a3FTUsK6/vlxeNILzsdZP3XTJ63COKKwoXYBQ8vryvJ+/JZklpaTTc864e5sYEIR/aJtH1e8uLxG7hH7kFAaC5NP4RIWdkm/P7/v9Rb2nfF+HFbW2QLbO3wCeXGh/ZC9mJLQn6tgSEjb8cV59dRkLP18+CyD6yGqIitV5pGejMZ1dK4YWUjo5uj4ZEIxMOBGKkKsFiSW+/6aG1NXj7qFExG8k8jgv4c1GKIaEo2IgAa9Vb6QiL/bnDaXPD3StfOLc1xfIo3dz+ywt/RbB6SEW8Oxv2ktj+nonrOswiKk1ecSbaLX1LwyuC0tAsnTmIJgztb4Ot78MQx7bCmYYqWB/vCkOr3zIc9zacuE7DUAXsI5bxPWFHn/KIPx515Ui3NHxb9xG+Yqg9wXbDKQoZYhykvmt4zq4QL/uKyW35W5YnROQgKd+jFyHWlgvD1Xz+6BcMx3lVTJ+3FYbikPUaOnj2vk0KhnxSuIsI0e7SvXy3jRteSCierG/dpdNnfgJmu3qv4SrpvvCxhhO7S2P28ciNB6ckWeHcUA6hCMN+JFv3bxn62xZje6zV8OqwpnjdSxgScQfOfmBoUJ9hcYbbYKEMp5zjy7bK0O89ZPTe7snw6jXUDCW7asN1nsm2HpcMS3NR9T2Hu0EsXkKMWSG27KPPn8tWHA926jmUJJWGJaShF6XgblKgE6arawlLMaUz/jKlVpfy0dMuopSX8YvXsMLQ2z8xOksBbctPZ762FsWIoGJrkbWH4kYShrwabL2Nvm444c1faEwC+DPjqdiiayX7U4If3RHvRln6NHqv7fO6NI7Fe3DxYCC6gjxQNBM9xjOjZlvUYVhMktERY/5fbA/TPo2IMd5nz8KQBxf3bTj4liHNKuZHEa6IW/C+Db93DamZmDMW3VphmDjt4VgZ9vf7ZX2Gq+3h6G4Hh8XhMBgsFov0b/p5cHQHi51WlxrP4SX9UDb0wpm6YGNH/jKiMMRiiEMaPoVRuzZDByPWHK89tDwdpjM0u+zeOrMobRLfn0WDZqtL096lWzSchb04b9ingRyHkYZEXPTMMApqNHQQrxdafJJ0xAu7ECkXG6IZSkR72MnqfG4Yij6NeMLkjOGxJ0dg78CQiPzXzYyd/9Yzv/mWYnDKMSJgjujTPMkJYvZLdgEy0nG8bFSkNecBpjDsztCzZviKEKnNMNrzO2uLRH4s4f2bWDxlB1QyFNfwIn+J6ISd4MnsVHdQ/nMTx5ODpaEvA39pmI0D1GHoOaL8y0ikymz4ufWReM3kMbQb9kW7sEPsH+h+/mTv0Q/l3CU3by3kS+knKtOY1jP+IM3FEBxPcEK8q9IeikFf0VqQy8PDZi+q2vyNfnewwWwuPyRZhp9PvJsbOvjDZy88OzLCWfNkkukGE1bBiiTQdZyGGfuAz0Nn7WEUhnJOV76u4U9SvVAMAnuoLwasHsgdtPhOiLctxEqGd90nRL325CFK60Hc/c+RyQdp+EGzPAuRyaEPyadPrn9IzijT40TocSFyAEXPe/10ObMTKHvevcuF17+19LxZMoHMgcAk4ouiqMT6u8UBi3oue33Nsk8RKc1CDNFyzX6ZyWtf0m+0wyjk8dKww76fxozBpbbo6ZsELKAtrKnYU8wk8gjYk98M1PejuiJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4N/mf2t87XfYQK2YAAAAAElFTkSuQmCC"}
                                        className={classes.imgPartner} />
                                </Grid>
                                <Grid item xs={3} lg={2} className={classes.gridImgPartner}>
                                    <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAmVBMVEV/ABn///91AAB4AAB+ABb79/h+ABR7AAB3AADr296GHyyMLzrCnqF8AAq2hYt9AA+jYGiXRlDawsV+AA2wfIH27e+qb3bQsbXv5ObUub2cVFxsAACeWWHhzM+SPEeLLDi8j5SueH7Lqa2nanDl0tVpAACygYXDm6CJIzGCDyDt3+HeyMvz6erDnaG6jJHWtruQN0GRQEmVSlL3ksxHAAAJUUlEQVR4nO2caXuiOhSAIQESdRDXKlUrLq21Wq/2//+4SxZIAsG20xl0eM77oY8slrwsSU5y0HEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAMgSX8D7bIbBtyL8VmeuH1sMWdor4ytB2mJ9Ct36RMdZ3wJPSDm6HHRvH5spAKg57rrH+hVgOS7rmTnOmSHbmyvYfUUQtt0RH/8/orWIHGpsrI2kY9sz1VYYGc3al8S9z5d8zfNAvIi5v//cN15ph0Gmi4QJpRVk10XBM1XY8aaKhu1f/Gm0badgL1Xa/FsN+VK/hLq9qgrZlcwMMB/mDGG6aaTjNK1P83kxDl2SdTHRoqOFHlG0eN9TwJEsVOLatTTCMZVUTPTbVsCWrGpI01dCV15AOGmv4JKoaW3D4TxuqzyNeLI/kK6bNMPwv74SKEDF6UpuaYRjnC1te1ZCXfNPptw2NEYObG+bBoM+rGhUc+g+/bxhaDnszQ3XcV/bfVXC4bYjhoJ9/3LByqeBw3RRDlH9kIWLwmi++rBpi+HzMPh7SBxGrC3exGBZGcP4Nw1keLB3TyhSv8y141BRDFfB6noMW2cJ01hRDpAYt5kMtOBygxhiqgacTCfb5wq4xhpTmly2mWlF7tDGGqo1/Q3iXb9iTxhhq1SdVweEYNchwpQ6K8ohp0SDD4JwvjFRwuMbNMXSw6opeVCGbZKiWt0rqNWiQoSq3n1c6PnIaZKgdOm8aWz81JKUckhsaRn23xAT/zBAnE0V8CW5rqA2v5XTJzwzNEUs+/XpDQwdN3SKX6IeGxkz5/NaGlmHu0GuUYXmqgs2XNslwOC8acu8GGXpe0TDBzTIsT/uyXJcvGDqyPMXkhjs0XJjH4flDFsNitlRfzoxrYSVnc3eGxfSLMZvCsBkWTGIxq+oV74HHyDxnN28PS3fZospwZe7nPqB0dTQrJsK1AzOhI+E3SmGvc52G2kg35xe2GwbLgoo7eaW0X8yDS28Bc3Tcb1OCLoW9eCZdXYZaiMjhMxgWQ1smiiUJjvXmC03sbrUu7OTzO7w2w8LEN5+FshlaEzNLpCco+vhsp0W9htg4w2Im0WYYnd3P4fUUtSU46oh54toMzbRgkXhiM3SQPVPDoEtK58yCyDKrzVCbvXd5cFhlaE/NNHjj50cbPbfyS0yE12ZohogiK8NqqE+/VbAU3QBszzyS+DJRsDZDM4uGt1QVhg6yJmcq5lmWgjWZOuNDjuTUZ2i0X+L8Vhh+otjL0zA8XA6sM/JkjfoM9S6VTKatMnRQ9Y06ftLf2yBVV3GTJyTXZ6i3XzJLsdLQwW1rki0bKDfe6QrQzrbXW0edhr9kqD9zB6HjhWpVIjKatNyhgmFa9L7FcdKmXuFQ9LXUuoxXKFI7FA1f/4ihFwwV2etnQZStibICqFXpykLZI+ScFlqjPh48YGopXYD2O+2WGcc9ZGaE6QcpH+a3FTUsK6/vlxeNILzsdZP3XTJ63COKKwoXYBQ8vryvJ+/JZklpaTTc864e5sYEIR/aJtH1e8uLxG7hH7kFAaC5NP4RIWdkm/P7/v9Rb2nfF+HFbW2QLbO3wCeXGh/ZC9mJLQn6tgSEjb8cV59dRkLP18+CyD6yGqIitV5pGejMZ1dK4YWUjo5uj4ZEIxMOBGKkKsFiSW+/6aG1NXj7qFExG8k8jgv4c1GKIaEo2IgAa9Vb6QiL/bnDaXPD3StfOLc1xfIo3dz+ywt/RbB6SEW8Oxv2ktj+nonrOswiKk1ecSbaLX1LwyuC0tAsnTmIJgztb4Ot78MQx7bCmYYqWB/vCkOr3zIc9zacuE7DUAXsI5bxPWFHn/KIPx515Ui3NHxb9xG+Yqg9wXbDKQoZYhykvmt4zq4QL/uKyW35W5YnROQgKd+jFyHWlgvD1Xz+6BcMx3lVTJ+3FYbikPUaOnj2vk0KhnxSuIsI0e7SvXy3jRteSCierG/dpdNnfgJmu3qv4SrpvvCxhhO7S2P28ciNB6ckWeHcUA6hCMN+JFv3bxn62xZje6zV8OqwpnjdSxgScQfOfmBoUJ9hcYbbYKEMp5zjy7bK0O89ZPTe7snw6jXUDCW7asN1nsm2HpcMS3NR9T2Hu0EsXkKMWSG27KPPn8tWHA926jmUJJWGJaShF6XgblKgE6arawlLMaUz/jKlVpfy0dMuopSX8YvXsMLQ2z8xOksBbctPZ762FsWIoGJrkbWH4kYShrwabL2Nvm444c1faEwC+DPjqdiiayX7U4If3RHvRln6NHqv7fO6NI7Fe3DxYCC6gjxQNBM9xjOjZlvUYVhMktERY/5fbA/TPo2IMd5nz8KQBxf3bTj4liHNKuZHEa6IW/C+Db93DamZmDMW3VphmDjt4VgZ9vf7ZX2Gq+3h6G4Hh8XhMBgsFov0b/p5cHQHi51WlxrP4SX9UDb0wpm6YGNH/jKiMMRiiEMaPoVRuzZDByPWHK89tDwdpjM0u+zeOrMobRLfn0WDZqtL096lWzSchb04b9ingRyHkYZEXPTMMApqNHQQrxdafJJ0xAu7ECkXG6IZSkR72MnqfG4Yij6NeMLkjOGxJ0dg78CQiPzXzYyd/9Yzv/mWYnDKMSJgjujTPMkJYvZLdgEy0nG8bFSkNecBpjDsztCzZviKEKnNMNrzO2uLRH4s4f2bWDxlB1QyFNfwIn+J6ISd4MnsVHdQ/nMTx5ODpaEvA39pmI0D1GHoOaL8y0ikymz4ufWReM3kMbQb9kW7sEPsH+h+/mTv0Q/l3CU3by3kS+knKtOY1jP+IM3FEBxPcEK8q9IeikFf0VqQy8PDZi+q2vyNfnewwWwuPyRZhp9PvJsbOvjDZy88OzLCWfNkkukGE1bBiiTQdZyGGfuAz0Nn7WEUhnJOV76u4U9SvVAMAnuoLwasHsgdtPhOiLctxEqGd90nRL325CFK60Hc/c+RyQdp+EGzPAuRyaEPyadPrn9IzijT40TocSFyAEXPe/10ObMTKHvevcuF17+19LxZMoHMgcAk4ouiqMT6u8UBi3oue33Nsk8RKc1CDNFyzX6ZyWtf0m+0wyjk8dKww76fxozBpbbo6ZsELKAtrKnYU8wk8gjYk98M1PejuiJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4N/mf2t87XfYQK2YAAAAAElFTkSuQmCC"}
                                        className={classes.imgPartner} />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Container>
                </Paper>
            </Grid>

            <Fab className={classes.fixedList}>
                <ViewCompactIcon className={classes.viewIcon} />
            </Fab>

            <Fab className={classes.fixedChat}>
                <img src={"https://www.oliviaoptic.co.th/wp-content/uploads/2019/11/messenger-512.png"} className={classes.imgMessenger} />
            </Fab>

            <CopyRight />



        </div>

    );
}

export default PartnerPage;

const useStyles = makeStyles((theme) => ({
    divbody: {
        // backgroundColor: "rgb(235 , 247 , 245)"
        backgroundColor : "#FAFAFA"
    },
    fab: {
        margin: theme.spacing(2),
    },
    fixedChat: {
        position: 'fixed',
        width: 60,
        height: 60,
        bottom: 29,
        right: 10,

    },
    fixedList: {
        position: 'fixed',
        bottom: 100,
        right: 14,
        maxWidth: 60,
        maxHeight: 60,
        backgroundColor: "#000000",
    },
    styleChip: {
        width: "5vw",
        height: "2vw",
        maxHeight: 23,
        maxWidth: 68,
        minHeight: 16,
        minWidth: 67,
        fontSize: "min(max(0.6vw,13px),2.5vw)",
        backgroundColor: "#157575",
        color: "#FFFFFF"
    },
    textDescription: {
        fontFamily: "Prompt, sans-serif",
        fontSize: "min(max(1vw,15px),3.5vw)"
    },
    textHeader: {
        fontSize: "min(max(1.5vw,20px),4vw)",
        fontFamily: "Prompt, sans-serif",
        fontWeight: "bold"
    },
    fixedGallery: {
        minWidth: 50,
        maxWidth: "15vw",
        maxHeight: "26vw",
        width: "28vw",
        height: "53vw",
        marginLeft: 50
    },
    containerBreadcrumbs: {
        width: "100%",
        paddingBottom: 45
    },
    divbreadcrumbsStyle: {
        paddingTop: 27,
        backgroundColor: "#000000"
    },
    breadcrumbsStyle: {
        color: "#FFFFFF",
        fontSize: 13
    },
    linkBreadcrumbs: {
        color: "#D5D5D5",
        fontFamily: "Prompt, sans-serif"
    },
    linkBreadcrumbsApp: {
        color: "#FFFFFF",
        fontSize: 13,
        fontFamily: "Prompt, sans-serif"
    },
    gridHeader: {
        backgroundColor: "#000000",
        paddingBottom: 9
    },
    h1nameApp: {
        fontSize: "min(max(2.5vw,26px),4.5vw)",
        color: "#FFFFFF",
        marginBottom: 0,
        fontFamily: "Prompt, sans-serif"
    },
    pTypeApp: {
        fontSize: "min(max(1.2vw,18px),3.5vw)",
        color: "#FFFFFF",
        marginTop: 0,
        marginBottom: 0,
        fontFamily: "Prompt, sans-serif"
    },
    pDescriptionApp: {
        fontSize: "min(max(0.8vw,18px),3.5vw)",
        color: "#FFFFFF",
        marginTop: 14,
        marginBottom: 0,
        fontFamily: "Prompt, sans-serif",
    },
    AppStoreStyle: {
        maxHeight: 145,
        maxWidth: 143,
        width: "25vw",
        marginRight: 10
    },
    playStore: {
        maxHeight: 145,
        maxWidth: 152,
        width: "25vw",
        marginRight: 10
    },
    imgPhone: {
        maxWidth: 206,
        maxHeight: 295,
        marginRight: 10,
        width: "27vw",
        height: "40vw",
        marginBottom: 70
    },
    gridBody: {
        paddingBottom: 22,
        width: "100%",
    },
    paperStyle: {
        backgroundColor: "#FFFFFF",
        marginLeft: "5vw",
        marginRight: "5vw",
        marginTop: -22
    },
    gridInfoPaper: {
        paddingTop: 32
    },
    gridLineInfo: {
        backgroundColor: "#E9F1F1",
        width: 4,
        height: 41
    },
    gridInfoText: {
        marginLeft: 30,
        marginRight: 30,
        textAlign: "center"
    },
    infoText: {
        fontFamily: "Prompt, sans-serif",
        fontSize: "min(max(2.5vw,20px),6vw)"
    },
    containerAvailable: {
        paddingBottom: 68
    },
    gridIconApp: {
        textAlign: "end"
    },
    imgIconApp: {
        minWidth: 65,
        minHeight: 65,
        width: "6vw",
        height: "6vw",
        borderRadius: 10
    },
    gridSlider: {
        marginLeft: "5vw",
        marginRight: "5vw"
    },
    imgSlider: {
        width: "96%",
        height: "100%"
    },
    gridPartner: {
        marginTop: 37
    },
    gridImgPartner: {
        marginRight: 16
    },
    imgPartner: {
        maxWidth: 140,
        width: "20vw",
        height: "20vw",
        maxHeight: 130,
        marginRight: 10
    },
    viewIcon: {
        color: "#FFFFFF"
    },
    imgMessenger: {
        width: "100%",
        maxHeight: 70,
        maxWidth: 70,
        height: "100%",
        borderRadius: 300
    },
    gridGallery: {
        marginTop: 30
    },
    gridChip: {
        marginRight: 8
    }

}));


