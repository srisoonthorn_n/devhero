import styled from 'styled-components';


export const ArrowSlider = styled.div`

.slick-prev:before, .slick-next:before {
    color : #5B5B5B;
    font-size: 24px;
}


.slick-track {
    width: 1000% !important;
}
.slick-slide {
  
}

.slick-next {
    right: -16px;
}

.slick-prev {
    left: -25px;
}
`

