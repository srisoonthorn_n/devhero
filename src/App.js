import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router , Route} from 'react-router-dom';
import ProjectPage from './components/Project_Page/ProjectPage';
import PartnerPage from './components/Partners_Page/PartnersPage';

function App() {
  return (
    <Router>
      <Route path="/" exact component={ProjectPage} />
      <Route path="/project" component={ProjectPage} />
      <Route path="/partner" component={PartnerPage} />
    </Router>
  );
}

export default App;
